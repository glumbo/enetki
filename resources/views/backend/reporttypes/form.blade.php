<div class="box-body">
    @if(isset($parents) && $parents->count()>0)
    <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
        {{ Form::label('parent_id', _tr('labels.backend.reporttypes.table.parent'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('parent_id', $parents, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.reporttypes.table.parent')]) }}
           @if ($errors->has('parent_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    @endif
    <div class="form-group">
         {{ Form::label('name', _tr('labels.backend.reporttypes.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.reporttypes.table.name'), 'required' => 'required']) }} 
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
