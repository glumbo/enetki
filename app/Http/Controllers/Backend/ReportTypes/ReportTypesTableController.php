<?php

namespace App\Http\Controllers\Backend\ReportTypes;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\ReportTypes\ReportTypeRepository;
use App\Http\Requests\Backend\ReportTypes\ManageReportTypeRequest;

/**
 * Class ReportTypesTableController.
 */
class ReportTypesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var ReportTypeRepository
     */
    protected $reporttype;

    /**
     * contructor to initialize repository object
     * @param ReportTypeRepository $reporttype;
     */
    public function __construct(ReportTypeRepository $reporttype)
    {
        $this->reporttype = $reporttype;
    }

    /**
     * This method return the data of the model
     * @param ManageReportTypeRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageReportTypeRequest $request)
    {
        return Datatables::of($this->reporttype->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('parent', function ($reporttype) {
                return isset($reporttype->parent->name)?$reporttype->parent->name:null;
            })
            ->addColumn('created_at', function ($reporttype) {
                return Carbon::parse($reporttype->created_at)->toDateString();
            })
            ->addColumn('actions', function ($reporttype) {
                return $reporttype->action_buttons;
            })
            ->make(true);
    }
}
