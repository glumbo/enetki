<?php

namespace App\Models\Reports\Traits;
use App\Models\ReportTypes\ReportType;
use App\Models\Access\User\User;

/**
 * Class ReportRelationship
 */
trait ReportRelationship
{

    public function type()
    {
        return $this->belongsTo(ReportType::class,'type_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
