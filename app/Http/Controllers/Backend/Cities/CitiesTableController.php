<?php

namespace App\Http\Controllers\Backend\Cities;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Cities\CityRepository;
use App\Http\Requests\Backend\Cities\ManageCityRequest;

/**
 * Class CitiesTableController.
 */
class CitiesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var CityRepository
     */
    protected $city;

    /**
     * contructor to initialize repository object
     * @param CityRepository $city;
     */
    public function __construct(CityRepository $city)
    {
        $this->city = $city;
    }

    /**
     * This method return the data of the model
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCityRequest $request)
    {
        return Datatables::of($this->city->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('country', function ($city) {
                return $city->country?$city->country->name:'';
            })
            ->addColumn('created_at', function ($city) {
                return Carbon::parse($city->created_at)->toDateString();
            })
            ->addColumn('actions', function ($city) {
                return $city->action_buttons;
            })
            ->make(true);
    }
}
