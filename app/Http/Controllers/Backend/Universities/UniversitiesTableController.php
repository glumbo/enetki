<?php

namespace App\Http\Controllers\Backend\Universities;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Universities\UniversityRepository;
use App\Http\Requests\Backend\Universities\ManageUniversityRequest;

/**
 * Class UniversitiesTableController.
 */
class UniversitiesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var UniversityRepository
     */
    protected $university;

    /**
     * contructor to initialize repository object
     * @param UniversityRepository $university;
     */
    public function __construct(UniversityRepository $university)
    {
        $this->university = $university;
    }

    /**
     * This method return the data of the model
     * @param ManageUniversityRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageUniversityRequest $request)
    {
        return Datatables::of($this->university->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('city', function ($university) {
                return $university->city?$university->city->name:'';
            })
            ->addColumn('created_at', function ($university) {
                return Carbon::parse($university->created_at)->toDateString();
            })
            ->addColumn('actions', function ($university) {
                return $university->action_buttons;
            })
            ->make(true);
    }
}
