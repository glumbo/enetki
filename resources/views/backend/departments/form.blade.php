<div class="box-body">
    <div class="form-group {{ $errors->has('faculty_id') ? ' has-error' : '' }}">
        {{ Form::label('faculty_id', _tr('labels.backend.departments.table.faculty'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('faculty_id', $faculties, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.departments.table.faculty'), 'required' => 'required']) }}
           @if ($errors->has('faculty_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('faculty_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('name', _tr('labels.backend.departments.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.departments.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('status', _tr('labels.backend.videos.table.status'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.videos.table.status')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
