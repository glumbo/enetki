<?php

namespace App\Http\Responses\Backend\Socials;

use Illuminate\Contracts\Support\Responsable;

class CreateResponse implements Responsable
{
    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $types = [
            'facebook' => _tr('labels.backend.socials.table.facebook'),
            'twitter' => _tr('labels.backend.socials.table.twitter'),
            'linkedin' => _tr('labels.backend.socials.table.linkedin'),
            'youtube' => _tr('labels.backend.socials.table.youtube'),
            'pinterest' => _tr('labels.backend.socials.table.pinterest'),
        ];
        $status = [
            0 => _tr('labels.general.inactive'),
            1 => _tr('labels.general.active'),
        ];
        return view('backend.socials.create',
        [
            'types' => $types,
            'status' => $status,
        ]
        );
    }
}