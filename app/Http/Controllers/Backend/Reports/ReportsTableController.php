<?php

namespace App\Http\Controllers\Backend\Reports;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Reports\ReportRepository;
use App\Http\Requests\Backend\Reports\ManageReportRequest;
use App\Models\Lessons\Lesson;

/**
 * Class ReportsTableController.
 */
class ReportsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var ReportRepository
     */
    protected $report;
    protected $statuses;

    /**
     * contructor to initialize repository object
     * @param ReportRepository $report;
     */
    public function __construct(ReportRepository $report)
    {
        $this->report = $report;
        $this->statuses = $this->types = [
            0 => _tr('labels.backend.reports.table.pending'), 
            1 => _tr('labels.backend.reports.table.done'), 
            2 => _tr('labels.backend.reports.table.processing'), 
            3 => _tr('labels.backend.reports.table.dummy'), 
        ];
    }

    /**
     * This method return the data of the model
     * @param ManageReportRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageReportRequest $request)
    {
        return Datatables::of($this->report->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('status', function ($report) {
                return isset($this->statuses[$report->status])?$this->statuses[$report->status]:null;
            })
            ->addColumn('type', function ($report) {
                return isset($report->type)?$report->type->name:null;
            })
            ->addColumn('user', function ($report) {
                return isset($report->user)?$report->user->name:null;
            })
            ->addColumn('data', function ($report) {
                $lesson = Lesson::find($report->data);
                return isset($lesson)?$lesson->name:null;
            })
            ->addColumn('created_at', function ($report) {
                return Carbon::parse($report->created_at)->toDateString();
            })
            ->addColumn('actions', function ($report) {
                return $report->action_buttons;
            })
            ->make(true);
    }
}
