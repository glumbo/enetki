<?php

namespace App\Http\Controllers\Backend\Answers;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Answers\AnswerRepository;
use App\Http\Requests\Backend\Answers\ManageAnswerRequest;

/**
 * Class AnswersTableController.
 */
class AnswersTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var AnswerRepository
     */
    protected $answer;
    protected $options;

    /**
     * contructor to initialize repository object
     * @param AnswerRepository $answer;
     */
    public function __construct(AnswerRepository $answer)
    {
        $this->answer = $answer;
        $this->options = [
            'a' => _tr('labels.backend.questions.table.a'), 
            'b' => _tr('labels.backend.questions.table.b'), 
            'c' => _tr('labels.backend.questions.table.c'), 
            'd' => _tr('labels.backend.questions.table.d'), 
            'e' => _tr('labels.backend.questions.table.e'), 
        ];
    }

    /**
     * This method return the data of the model
     * @param ManageAnswerRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageAnswerRequest $request)
    {
        return Datatables::of($this->answer->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('quiz', function ($answer) {
                return $answer->quiz?$answer->quiz->name:'';
            })
            ->addColumn('user', function ($answer) {
                return $answer->user?$answer->user->first_name.' '.$answer->user->last_name:'';
            })
            ->addColumn('question', function ($answer) {
                return $answer->question?$answer->question->content:'';
            })
            ->addColumn('answer', function ($answer) {
                return $this->options[$answer->answer]?$this->options[$answer->answer]:'';
            })
            ->addColumn('created_at', function ($answer) {
                return Carbon::parse($answer->created_at)->toDateString();
            })
            ->addColumn('actions', function ($answer) {
                return $answer->action_buttons;
            })
            ->make(true);
    }
}
