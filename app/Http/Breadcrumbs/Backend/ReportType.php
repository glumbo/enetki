<?php

Breadcrumbs::register('admin.reporttypes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.reporttypes.management'), route('admin.reporttypes.index'));
});

Breadcrumbs::register('admin.reporttypes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.reporttypes.index');
    $breadcrumbs->push(trans('menus.backend.reporttypes.create'), route('admin.reporttypes.create'));
});

Breadcrumbs::register('admin.reporttypes.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.reporttypes.index');
    $breadcrumbs->push(trans('menus.backend.reporttypes.edit'), route('admin.reporttypes.edit', $id));
});
