<?php
return [
	"pages" => [
	"table" => "pages",
	],
	"email_templates" => [
	"table" => "email_templates",
	"placeholders_table" => "email_template_placeholders",
	"types_table" => "email_template_types",
	],
	"blog_tags" => [
	"table" => "blog_tags",
	],
	"blog_categories" => [
	"table" => "blog_categories",
	],
	"blogs" => [
	"table" => "blogs",
	],
	"faqs" => [
	"table" => "faqs",
	],
	"contacts" => [
	"table" => "contacts",
	],
	"navbars" => [
	"table" => "navbars",
	],
	"cards" => [
	"table" => "cards",
	],
	"projects" => [
	"table" => "projects",
	],
	"members" => [
	"table" => "members",
	],
	"sliders" => [
	"table" => "sliders",
	],
	"testimonials" => [
	"table" => "testimonials",
	],
	"references" => [
	"table" => "references",
	],
	"subscribes" => [
	"table" => "subscribes",
	],
];