@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.sliders.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.sliders.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.sliders.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.sliders.partials.sliders-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="sliders-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.sliders.table.id') }}</th>
                            <th>{{ _tr('labels.backend.sliders.table.cover') }}</th>
                            <th>{{ _tr('labels.backend.sliders.table.subtitle') }}</th>
                            <th>{{ _tr('labels.backend.sliders.table.title') }}</th>
                            <th>{{ _tr('labels.backend.sliders.table.status') }}</th>
                            <th>{{ _tr('labels.backend.sliders.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    @include('includes.datatables')

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#sliders-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.sliders.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.sliders.table')}}.id'},
                    {data: 'cover', name: '{{config('module.sliders.table')}}.cover'},
                    {data: 'subtitle', name: '{{config('module.sliders.table')}}.subtitle'},
                    {data: 'title', name: '{{config('module.sliders.table')}}.title'},
                    {data: 'status', name: '{{config('module.sliders.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.sliders.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
