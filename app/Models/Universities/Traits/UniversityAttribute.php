<?php

namespace App\Models\Universities\Traits;

/**
 * Class UniversityAttribute.
 */
trait UniversityAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return "<div class=\"btn-group action-btn\"> {$this->getEditButtonAttribute("edit-university", "admin.universities.edit")}
                {$this->getDeleteButtonAttribute("delete-university", "admin.universities.destroy")}
                </div>";
    }
}
