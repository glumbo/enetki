<?php

namespace App\Repositories\Backend\Lessons;

use DB;
use Carbon\Carbon;
use App\Models\Lessons\Lesson;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class LessonRepository.
 */
class LessonRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Lesson::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.lessons.table').'.id',
                config('module.lessons.table').'.department_id',
                config('module.lessons.table').'.parent_id',
                config('module.lessons.table').'.teacher_id',
                config('module.lessons.table').'.video_id',
                config('module.lessons.table').'.name',
                config('module.lessons.table').'.cover',
                config('module.lessons.table').'.description',
                config('module.lessons.table').'.price',
                config('module.lessons.table').'.created_at',
                config('module.lessons.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $input['slug'] = Str::slug($input['name']);
        if (Lesson::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.lessons.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Lesson $lesson
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Lesson $lesson, array $input)
    {
        $input['slug'] = Str::slug($input['name']);
    	if ($lesson->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.lessons.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Lesson $lesson
     * @throws GeneralException
     * @return bool
     */
    public function delete(Lesson $lesson)
    {
        if ($lesson->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.lessons.delete_error'));
    }
}
