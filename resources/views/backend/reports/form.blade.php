<div class="box-body">
    <div class="form-group">
        <label class="col-lg-2 control-label">{{ _tr('labels.backend.reports.table.user') }}</label>
        <div class="col-lg-10">
            {{ Form::text(null, $reports->user->name, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.reports.table.data'), 'disabled' => 'true']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        <label class="col-lg-2 control-label">{{ _tr('labels.backend.reports.table.type') }}</label>
        <div class="col-lg-10">
            {{ Form::text(null, $reports->type->name, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.reports.table.type'), 'disabled' => 'true']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('data', _tr('labels.backend.reports.table.data'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('data', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.reports.table.data'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('subject', _tr('labels.backend.reports.table.subject'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('subject', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.reports.table.subject')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('content', _tr('labels.backend.reports.table.content'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('content', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.reports.table.content')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('comment', _tr('labels.backend.reports.table.comment'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('comment', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.reports.table.comment')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('status', _tr('labels.backend.reports.table.status'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('status', $statuses, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.reports.table.status')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
