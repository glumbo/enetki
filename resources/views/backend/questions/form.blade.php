<div class="box-body">
<div class="form-group {{ $errors->has('lesson_id') ? ' has-error' : '' }}">
        {{ Form::label('lesson_id', _tr('labels.backend.questions.table.lesson'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('lesson_id', $lessons, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.questions.table.lesson')]) }}
           @if ($errors->has('lesson_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('lesson_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('video_id') ? ' has-error' : '' }}">
        {{ Form::label('video_id', _tr('labels.backend.questions.table.video'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('video_id', $videos, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.questions.table.video')]) }}
           @if ($errors->has('video_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('video_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
        {{ Form::label('type', _tr('labels.backend.questions.table.type'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('type', $types, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.questions.table.type')]) }}
           @if ($errors->has('type'))
                <span class="help-block">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('content', _tr('labels.backend.questions.table.content'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('content', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.content')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('a', _tr('labels.backend.questions.table.a'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('a', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.a')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('b', _tr('labels.backend.questions.table.b'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('b', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.b')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('c', _tr('labels.backend.questions.table.c'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('c', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.c')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('d', _tr('labels.backend.questions.table.d'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('d', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.d')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('e', _tr('labels.backend.questions.table.e'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('e', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.questions.table.e')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('right_answer') ? ' has-error' : '' }}">
        {{ Form::label('right_answer', _tr('labels.backend.questions.table.right_answer'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('right_answer', $options, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.questions.table.right_answer')]) }}
           @if ($errors->has('right_answer'))
                <span class="help-block">
                    <strong>{{ $errors->first('right_answer') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    

</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        Backend.Questions.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
    </script
@endsection
