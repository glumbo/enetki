<div class="box-body">
    <div class="form-group {{ $errors->has('quiz_id') ? ' has-error' : '' }}">
        {{ Form::label('quiz_id', _tr('labels.backend.quizquestions.table.quiz'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('quiz_id', $quizzes, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.quizquestions.table.quiz'), 'required' => 'required']) }}
           @if ($errors->has('quiz_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('quiz_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('question_id', _tr('labels.backend.quizquestions.table.question'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::number('question_id', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.quizquestions.table.question'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('order', _tr('labels.backend.quizquestions.table.order'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('order', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.quizquestions.table.order'), 'min'=>'1', 'max'=>'30']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        $( document ).ready( function() {
            @if(isset($quiz))
                $("#quiz_id").val({{$quiz}});
            @endif
        });
    </script>
@endsection
