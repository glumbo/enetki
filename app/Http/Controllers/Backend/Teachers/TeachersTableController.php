<?php

namespace App\Http\Controllers\Backend\Teachers;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Teachers\TeacherRepository;
use App\Http\Requests\Backend\Teachers\ManageTeacherRequest;
use Illuminate\Support\Facades\Storage;

/**
 * Class TeachersTableController.
 */
class TeachersTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TeacherRepository
     */
    protected $teacher;

    /**
     * contructor to initialize repository object
     * @param TeacherRepository $teacher;
     */
    public function __construct(TeacherRepository $teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * This method return the data of the model
     * @param ManageTeacherRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTeacherRequest $request)
    {
        return Datatables::of($this->teacher->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('department', function ($teacher) {
                return $teacher->department?$teacher->department->name:'';
            })
            ->addColumn('name', function ($teacher) {
                return $teacher->title.' '.$teacher->first_name.' '.$teacher->last_name;
            })
            ->addColumn('cover', function ($teacher) {
                return $teacher->cover?'<img height="50" width="50" src="'.Storage::disk('public')->url($teacher->cover).'">':'';
            })
            ->addColumn('created_at', function ($teacher) {
                return Carbon::parse($teacher->created_at)->toDateString();
            })
            ->addColumn('actions', function ($teacher) {
                return $teacher->action_buttons;
            })
            ->make(true);
    }
}
