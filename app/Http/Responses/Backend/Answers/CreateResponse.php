<?php

namespace App\Http\Responses\Backend\Answers;

use Illuminate\Contracts\Support\Responsable;
use App\Models\Quizzes\Quiz;
use App\Models\Questions\Question;
use App\Models\Access\User\User;

class CreateResponse implements Responsable
{
    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $quizzes=collect(Quiz::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $questions=collect(Question::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['id']];
        });
        $users=collect(User::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['first_name'].' '.$item['last_name']];
        });
        $options = [
            'a' => _tr('labels.backend.questions.table.a'), 
            'b' => _tr('labels.backend.questions.table.b'), 
            'c' => _tr('labels.backend.questions.table.c'), 
            'd' => _tr('labels.backend.questions.table.d'), 
            'e' => _tr('labels.backend.questions.table.e'), 
        ];
        return view('backend.answers.create',[
            'quizzes' => $quizzes,
            'questions' => $questions,
            'users' => $users,
            'options' => $options,
        ]);
    }
}