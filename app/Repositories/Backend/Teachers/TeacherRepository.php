<?php

namespace App\Repositories\Backend\Teachers;

use DB;
use Carbon\Carbon;
use App\Models\Teachers\Teacher;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class TeacherRepository.
 */
class TeacherRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Teacher::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.teachers.table').'.id',
                config('module.teachers.table').'.department_id',
                config('module.teachers.table').'.title',
                config('module.teachers.table').'.first_name',
                config('module.teachers.table').'.last_name',
                config('module.teachers.table').'.slug',
                config('module.teachers.table').'.cover',
                config('module.teachers.table').'.biography',
                config('module.teachers.table').'.created_at',
                config('module.teachers.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $name = $input['title'].' '.$input['first_name'].' '.$input['last_name'];
        $input['slug'] = Str::slug($name);
        if (Teacher::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.teachers.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Teacher $teacher
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Teacher $teacher, array $input)
    {
        $name = $input['title'].' '.$input['first_name'].' '.$input['last_name'];
        $input['slug'] = Str::slug($name);
    	if ($teacher->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.teachers.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Teacher $teacher
     * @throws GeneralException
     * @return bool
     */
    public function delete(Teacher $teacher)
    {
        if ($teacher->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.teachers.delete_error'));
    }
}
