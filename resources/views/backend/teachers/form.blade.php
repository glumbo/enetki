<div class="box-body">
    <div class="form-group {{ $errors->has('department_id') ? ' has-error' : '' }}">
        {{ Form::label('department_id', _tr('labels.backend.teachers.table.department'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('department_id', $departments, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.teachers.table.department'), 'required' => 'required']) }}
           @if ($errors->has('department_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('department_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('title', _tr('labels.backend.teachers.table.title'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.teachers.table.title'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('first_name', _tr('labels.backend.teachers.table.first_name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('first_name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.teachers.table.first_name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('last_name', _tr('labels.backend.teachers.table.last_name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('last_name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.teachers.table.last_name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('cover', _tr('labels.backend.teachers.table.cover'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($teachers->cover))
            <div class="col-lg-1 file_show">
                <img src="{{ Storage::disk('public')->url($teachers->cover) }}" height="80" width="80">
                <i class="fal fa-times remove_file"></i>
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                    <input type="hidden" name="remove_file" id="remove_file" value="0" />
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('biography', _tr('labels.backend.teachers.table.biography'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('biography', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.teachers.table.biography')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
    Backend.Teachers.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');

    </script>
@endsection
