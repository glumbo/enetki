@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.quizquestions.management') . ' | ' . _tr('labels.backend.quizquestions.edit'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.quizquestions.management') }}
        <small>{{ _tr('labels.backend.quizquestions.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($quizquestions, ['route' => ['admin.quizquestions.update', $quizquestions], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-quizquestion']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.quizquestions.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.quizquestions.partials.quizquestions-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.quizquestions.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.quizquestions.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
