<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\DisableForeignKeys;
use Database\TruncateTable;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'backend',
                'name' => 'Backend Sidebar Menu',
                'items' => '[{"id":11,"name":"Access Management","url":"","url_type":"route","open_in_new_tab":0,"icon":"fa-users","view_permission_id":"view-access-management","content":"Access Management","children":[{"id":12,"name":"User Management","url":"admin.access.user.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-user-management","content":"User Management"},{"id":13,"name":"Role Management","url":"admin.access.role.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-role-management","content":"Role Management"},{"id":14,"name":"Permission Management","url":"admin.access.permission.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-permission-management","content":"Permission Management"}]},{"id":1,"name":"Module","url":"admin.modules.index","url_type":"route","open_in_new_tab":0,"icon":"fa-wrench","view_permission_id":"view-module","content":"Module"},{"id":3,"name":"Menus","url":"admin.menus.index","url_type":"route","open_in_new_tab":0,"icon":"fa-bars","view_permission_id":"view-menu","content":"Menus"},{"id":2,"name":"Pages","url":"admin.pages.index","url_type":"route","open_in_new_tab":0,"icon":"fa-file-text","view_permission_id":"view-page","content":"Pages"},{"id":9,"name":"Settings","url":"admin.settings.edit?setting=1","url_type":"route","open_in_new_tab":0,"icon":"fa-gear","view_permission_id":"edit-settings","content":"Settings"},{"id":15,"name":"Blog Management","url":"","url_type":"route","open_in_new_tab":0,"icon":"fa-commenting","view_permission_id":"view-blog","content":"Blog Management","children":[{"id":16,"name":"Blog Category Management","url":"admin.blogCategories.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-blog-category","content":"Blog Category Management"},{"id":17,"name":"Blog Tag Management","url":"admin.blogTags.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-blog-tag","content":"Blog Tag Management"},{"id":18,"name":"Blog Management","url":"admin.blogs.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-blog","content":"Blog Management"}]},{"id":19,"name":"Faq Management","url":"admin.faqs.index","url_type":"route","open_in_new_tab":0,"icon":"fa-question-circle","view_permission_id":"view-faq","content":"Faq Management"},{"view_permission_id":"view-navbar-permission","open_in_new_tab":0,"url_type":"route","url":"admin.navbars.index","name":"Navbars","id":20,"content":"Navbars"},{"id":21,"name":"Sliders","url":"admin.sliders.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-sliders-permission","content":"Sliders"},{"view_permission_id":"view-card-permission","open_in_new_tab":0,"url_type":"route","url":"admin.cards.index","name":"Cards","id":22,"content":"Cards"},{"id":23,"name":"Projects","url":"admin.projects.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-project-permission","content":"Projects"},{"view_permission_id":"view-member-permission","open_in_new_tab":0,"url_type":"route","url":"admin.members.index","name":"Members","id":24,"content":"Members"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'platform_id' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'backend',
                'name' => 'Backend Sidebar Menu',
                'items' => '[{"view_permission_id":"view-access-management","icon":"fa-users","open_in_new_tab":0,"url_type":"route","url":"","name":"Access Management","id":11,"content":"Access Management","children":[{"view_permission_id":"view-user-management","open_in_new_tab":0,"url_type":"route","url":"admin.access.user.index","name":"User Management","id":12,"content":"User Management"},{"view_permission_id":"view-role-management","open_in_new_tab":0,"url_type":"route","url":"admin.access.role.index","name":"Role Management","id":13,"content":"Role Management"},{"view_permission_id":"view-permission-management","open_in_new_tab":0,"url_type":"route","url":"admin.access.permission.index","name":"Permission Management","id":14,"content":"Permission Management"}]},{"view_permission_id":"view-module","icon":"fa-wrench","open_in_new_tab":0,"url_type":"route","url":"admin.modules.index","name":"Module","id":1,"content":"Module"},{"view_permission_id":"view-menu","icon":"fa-bars","open_in_new_tab":0,"url_type":"route","url":"admin.menus.index","name":"Menus","id":3,"content":"Menus"},{"view_permission_id":"view-page","icon":"fa-file-text","open_in_new_tab":0,"url_type":"route","url":"admin.pages.index","name":"Pages","id":2,"content":"Pages"},{"view_permission_id":"edit-settings","icon":"fa-gear","open_in_new_tab":0,"url_type":"route","url":"admin.settings.edit?setting=1","name":"Settings","id":9,"content":"Settings"},{"view_permission_id":"view-blog","icon":"fa-commenting","open_in_new_tab":0,"url_type":"route","url":"","name":"Blog Management","id":15,"content":"Blog Management","children":[{"view_permission_id":"view-blog-category","open_in_new_tab":0,"url_type":"route","url":"admin.blogCategories.index","name":"Blog Category Management","id":16,"content":"Blog Category Management"},{"view_permission_id":"view-blog-tag","open_in_new_tab":0,"url_type":"route","url":"admin.blogTags.index","name":"Blog Tag Management","id":17,"content":"Blog Tag Management"},{"view_permission_id":"view-blog","open_in_new_tab":0,"url_type":"route","url":"admin.blogs.index","name":"Blog Management","id":18,"content":"Blog Management"}]},{"view_permission_id":"view-faq","icon":"fa-question-circle","open_in_new_tab":0,"url_type":"route","url":"admin.faqs.index","name":"Faq Management","id":19,"content":"Faq Management"},{"id":20,"name":"Navbars","url":"admin.navbars.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-navbar-permission","content":"Navbars"},{"view_permission_id":"view-sliders-permission","open_in_new_tab":0,"url_type":"route","url":"admin.sliders.index","name":"Sliders","id":21,"content":"Sliders"},{"id":22,"name":"Cards","url":"admin.cards.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-card-permission","content":"Cards"},{"view_permission_id":"view-project-permission","open_in_new_tab":0,"url_type":"route","url":"admin.projects.index","name":"Projects","id":23,"content":"Projects"},{"id":24,"name":"Members","url":"admin.members.index","url_type":"route","open_in_new_tab":0,"view_permission_id":"view-member-permission","content":"Members"},{"view_permission_id":"view-testimonial-permission","open_in_new_tab":0,"url_type":"route","url":"admin.testimonials.index","name":"Testimonials","id":25,"content":"Testimonials"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => '2021-09-04 13:57:35',
                'deleted_at' => NULL,
                'platform_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'navbar',
                'name' => 'Navbar Menu',
                'items' => '[{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"route","url":"frontend.index","name":"Home","id":1,"content":"Home"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"route","url":"frontend.services","name":"Services","id":3,"content":"Services"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"route","url":"frontend.index","name":"About Us","id":2,"content":"About Us"},{"id":4,"name":"Projects","url":"frontend.projects","url_type":"route","open_in_new_tab":0,"icon":"","view_permission_id":"","content":"Projects"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"route","url":"frontend.contact","name":"Contact","id":5,"content":"Contact"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'platform_id' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'navbar',
                'name' => 'Navbar Menu',
                'items' => '[{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Product1","id":1,"content":"Product1"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Product2","id":3,"content":"Product2"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Product3","id":2,"content":"Product3"},{"id":4,"name":"Product4","url":"#","url_type":"static","open_in_new_tab":0,"icon":"","view_permission_id":"","content":"Product4"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Product5","id":5,"content":"Product5"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Product6","id":6,"content":"Product6"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'platform_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'sidebar',
                'name' => 'Sidebar Menu',
                'items' => '[{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Link1","id":1,"content":"Link1"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Link2","id":3,"content":"Link2"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Link3","id":2,"content":"Link3"},{"id":4,"name":"Link4","url":"#","url_type":"static","open_in_new_tab":0,"icon":"","view_permission_id":"","content":"Link4"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Link5","id":5,"content":"Link5"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Link6","id":6,"content":"Link6"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'platform_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'type' => 'footer',
                'name' => 'Footer Menu',
                'items' => '[{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Footer1","id":1,"content":"Footer1"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Footer2","id":3,"content":"Footer2"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Footer3","id":2,"content":"Footer3"},{"id":4,"name":"Footer4","url":"#","url_type":"static","open_in_new_tab":0,"icon":"","view_permission_id":"","content":"Footer4"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Footer5","id":5,"content":"Footer5"},{"view_permission_id":"","icon":"","open_in_new_tab":0,"url_type":"static","url":"#","name":"Footer6","id":6,"content":"Footer6"}]',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-08-30 19:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'platform_id' => 1,
            ),
        ));
        
        
    }
}