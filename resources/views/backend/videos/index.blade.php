@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.videos.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.videos.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.videos.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.videos.partials.videos-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="videos-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.videos.table.id') }}</th>
                            <th>{{ _tr('labels.backend.videos.table.site') }}</th>
                            <th>{{ _tr('labels.backend.videos.table.code') }}</th>
                            <th>{{ _tr('labels.backend.videos.table.type') }}</th>
                            <th>{{ _tr('labels.backend.videos.table.status') }}</th>
                            <th>{{ _tr('labels.backend.videos.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#videos-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.videos.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.videos.table')}}.id'},
                    {data: 'site', name: '{{config('module.videos.table')}}.site'},
                    {data: 'code', name: '{{config('module.videos.table')}}.code'},
                    {data: 'type', name: '{{config('module.videos.table')}}.type'},
                    {data: 'status', name: '{{config('module.videos.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.videos.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
