<?php

namespace App\Models\QuizQuestion\Traits;
use App\Models\Quizzes\Quiz;
use App\Models\Questions\Question;

/**
 * Class QuizQuestionRelationship
 */
trait QuizQuestionRelationship
{
    public function quiz()
    {
        return $this->belongsTo(Quiz::class,'quiz_id');
    }
    public function question()
    {
        return $this->belongsTo(Question::class,'question_id');
    }
}
