<li class="nav-item @if(!empty($item->children)) {{ 'has-treeview' }} @endif">
    <a class="nav-link {{ active_class(isActiveMenuItem($item)) }}" href="{{ getRouteUrl($item->url, $item->url_type) }}" @if(!empty($item->open_in_new_tab) &&
        ($item->open_in_new_tab == 1)) {{ 'target="_blank"' }} @endif class="nav-link">
        <i class="fal {{ @$item->icon }}"></i>
        <p>
            {{ $item->name }}
            @if (!empty($item->children))
            <i class="right fas fa-angle-left"></i>
            @endif
        </p>
    </a>
    @if (!empty($item->children))
    <ul class="nav nav-treeview {{ active_class(isActiveMenuItem($item), 'menu-open') }}" style="display: none; {{ active_class(isActiveMenuItem($item), 'display: block;') }}">
        {{ renderMenuItems($item->children) }}
    </ul>
    @endif
</li>