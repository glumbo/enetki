@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.reports.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.reports.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.reports.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.reports.partials.reports-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="reports-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.reports.table.id') }}</th>
                            <th>{{ trans('labels.backend.reports.table.type') }}</th>
                            <th>{{ trans('labels.backend.reports.table.user') }}</th>
                            <th>{{ trans('labels.backend.reports.table.data') }}</th>
                            <th>{{ trans('labels.backend.reports.table.subject') }}</th>
                            <th>{{ trans('labels.backend.reports.table.content') }}</th>
                            <th>{{ trans('labels.backend.reports.table.status') }}</th>
                            <th>{{ trans('labels.backend.reports.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#reports-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.reports.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.reports.table')}}.id'},
                    {data: 'type', name: '{{config('module.reports.table')}}.type_id'},
                    {data: 'user', name: '{{config('module.reports.table')}}.user_id'},
                    {data: 'data', name: '{{config('module.reports.table')}}.data'},
                    {data: 'subject', name: '{{config('module.reports.table')}}.subject'},
                    {data: 'content', name: '{{config('module.reports.table')}}.content'},
                    {data: 'status', name: '{{config('module.reports.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.reports.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "desc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
