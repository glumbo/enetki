@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.contacts.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.contacts.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.contacts.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.contacts.partials.contacts-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="contacts-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.contacts.table.id') }}</th>
                            <th>{{ trans('labels.backend.contacts.table.first_name') }}</th>
                            <th>{{ trans('labels.backend.contacts.table.last_name') }}</th>
                            <th>{{ trans('labels.backend.contacts.table.email') }}</th>
                            <th>{{ trans('labels.backend.contacts.table.subject') }}</th>
                            <th>{{ trans('labels.backend.contacts.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    @include('includes.datatables')

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#contacts-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.contacts.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.contacts.table')}}.id'},
                    {data: 'first_name', name: '{{config('module.contacts.table')}}.first_name'},
                    {data: 'last_name', name: '{{config('module.contacts.table')}}.last_name'},
                    {data: 'email', name: '{{config('module.contacts.table')}}.email'},
                    {data: 'subject', name: '{{config('module.contacts.table')}}.subject'},
                    {data: 'created_at', name: '{{config('module.contacts.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
