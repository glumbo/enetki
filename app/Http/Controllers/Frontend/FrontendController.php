<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Sliders\Slider;
use App\Models\Cards\Card;
use App\Models\Projects\Project;
use App\Models\Contacts\Contact;
use App\Models\Members\Member;
use App\Models\Testimonials\Testimonial;
use App\Models\References\Reference;
use App\Models\Subscribes\Subscribe;
use App\Models\Blogs\Blog;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $setting = Setting::first();
        $google_analytics = $setting->google_analytics;
        $sliders = Slider::where('status',1)->get();
        $services = Card::where('status',1)->where('type',0)->get();
        $solutions = Card::where('status',1)->where('type',1)->get();
        $projects = Project::where('status',1)->get();
        $members = Member::where('status',1)->get();
        $testimonials = Testimonial::all();
        $references = Reference::all();
        $blogs = Blog::limit(3)->get();

        return view('frontend.index', compact('google_analytics', 'sliders', 'services', 'solutions', 'projects', 'members', 'testimonials', 'references','blogs'));
    }

    /**
     * show page by $page_slug.
     */
    public function showPage($slug, PagesRepository $pages)
    {
        $result = $pages->findBySlug($slug);
        $pages = $pages->getAll();
        return view('frontend.pages.index',compact('pages'))
            ->withpage($result);
    }
    public function services()
    {
        $services = Card::where('type',0)->get();
        return view('frontend.services.index',compact('services'));
    }
    public function projects()
    {
        $projects = Project::all();
        return view('frontend.projects.index',compact('projects'));
    }
    public function contact()
    {
        return view('frontend.pages.contact');
    }

    public function mission()
    {
        return view('frontend.pages.mission');
    }
    
    public function contactSave(Request $request){
        $messages = [
            'first_name.required'    => 'Lütfen bu alanı doldurunuz*',
            'last_name.required'     => 'Lütfen bu alanı doldurunuz*',
            'email.required'         => 'Lütfen bu alanı doldurunuz*',
            'email.email'            => 'Lütfen email adresinizi giriniz*',
            'subject.required'       => 'Lütfen bu alanı doldurunuz*',
            'message.required'       => 'Lütfen bu alanı doldurunuz*',
        ];

        $validator = Validator::make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email',
            'subject'       => 'required',
            'message'       => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $contact = new Contact();
            $contact->first_name = $request->first_name??null;
            $contact->last_name = $request->last_name??null;
            $contact->email = $request->email??null;
            $contact->subject = $request->subject??null;
            $contact->message = $request->message??null;
            $contact->save();
            $request->session()->flash('status', 1);
            return redirect()->back();
        }catch (\Exception $exception){
            $request->session()->flash('status', 0);
            return redirect()->back();
        }
    }

    public function postSubscribe(Request $request)
    {
        $subscribe = [
            'email.required'         => 'Lütfen bu alanı doldurunuz*',
            'email.email'            => 'Lütfen email adresinizi giriniz*',
        ];

        $validator = Validator::make($request->all(), [
            'email'         => 'required|email',
        ], $subscribe);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $subscribe = new Subscribe();
            $subscribe->name = $request->name??null;
            $subscribe->email = $request->email??null;
            $subscribe->phone = $request->phone??null;
            $subscribe->save();

            $request->session()->flash('status', 1);
            return redirect()->back();
        }catch (\Exception $exception){
            $request->session()->flash('status', 0);
            return redirect()->back();
        }
    }
}
