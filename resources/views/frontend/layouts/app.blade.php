@php
use Illuminate\Support\Facades\Route;
@endphp
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en">
	<meta name="Enetki" />
	<meta name="keywords" content="enetki" />
	<meta name="description" content="Enetki" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Enetki" />
	<meta property="og:description" content="Enetki" />
	<meta property="og:site_name" content="Enetki" />
	<meta property="og:image" content="{{ assets('theme/static/images/share.jpg')}}" />
	<meta itemprop="name" content="Enetki" />
	<meta itemprop="image" content="{{ assets('theme/static/images/share.jpg')}}" />
	<meta property="og:url" content="{{ assets('') }}" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title', app_name())</title>



	<link rel="stylesheet" type="text/css" href="{{ assets('theme/style.min.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ assets('theme/fix.css') }}" media="screen">
	<script type="text/javascript" src="{{ assets('theme/static/deps-min.js') }}"></script>
	<script type="text/javascript" async src="{{ assets('theme/static/player-min.js') }}"></script>

	<style>
		@media (max-width: 700px) {
			#feature .background {}
		}
	</style>
</head>

<body id="home">
	<div id="wrapper">
		<div id="header" class="section" data-limit=".15">
			<div class="header-bg"></div>
			<div class="header-inner">
				<a href="{{ route('frontend.index') }}" id="logo">
					@if($setting->logo)
					<img src="{{ Storage::disk('public')->url('img/logo/' . $setting->logo) }}" alt="{{ app_name() }}" class="brand-image img-circle elevation-3" style="opacity: .8">
					@else
					<span class="brand-text font-weight-light">{{ app_name() }}</span>
					@endif
				</a>
				<div id="navigation">
					<ul class="nav-links">
						{{ renderMenuItems(getMenuItems('navbar') , 'frontend.includes.partials.menu-item', 'navbar-nav') }}
						{{-- <li><a href="#">One</a></li>
						<li><a href="#">Two</a>
						<ul class="dropdown">
							<li><a href="#">Sub-1</a></li>
							<li><a href="#">Sub-2</a></li>
							<li><a href="#">Sub-3</a></li>
						</ul>
						</li>
						<li><a href="#">Three</a></li> --}}
						  
					</ul>
				</div>
			</div>

			{{-- <div id="navigation-right">
				<ul class="nav-links">
					<li class="nav-item"><a href="#" rel="noopener" target="_blank">SHOP</a></li>
				</ul>
			</div> --}}

			<div id="menu-close"></div>
			<div id="menu">
				<div id="menu-background"></div>
				<div id="menu-navigation">
					<ul class="nav-links">
						{{ renderMenuItems(getMenuItems('navbar') , 'frontend.includes.partials.menu-item', 'navbar') }}
						{{ renderMenuItems(getMenuItems('sidebar') , 'frontend.includes.partials.menu-item', 'sidebar') }}
					</ul>
				</div>
			</div>
			<button id="hamburger" aria-expanded="false" aria-controls="menu" aria-label="Menu">
				<div id="bar1" class="bar"></div>
				<div id="bar2" class="bar"></div>
				<div id="bar3" class="bar"></div>
			</button>
		</div>
		@yield('content')

	</div>

	<script type="text/javascript" src="{{ assets('theme/static/core-min.js') }}"></script>
</body>

</html>