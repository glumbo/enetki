<?php

namespace App\Http\Responses\Backend\Cards;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Cards\Card
     */
    protected $cards;

    /**
     * @param App\Models\Cards\Card $cards
     */
    public function __construct($cards)
    {
        $this->cards = $cards;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $status = [
            0 => trans('labels.backend.cards.table.passive'),
            1 => trans('labels.backend.cards.table.active')
        ];
        $types = [
            0 => trans('labels.backend.cards.table.service'),
            1 => trans('labels.backend.cards.table.solution')
        ];
        return view('backend.cards.edit')->with([
            'cards' => $this->cards,
            'status' => $status,
            'types' => $types,
        ]);
    }
}