<?php

namespace App\Http\Controllers\Backend\Universities;

use App\Models\Universities\University;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Universities\CreateResponse;
use App\Http\Responses\Backend\Universities\EditResponse;
use App\Repositories\Backend\Universities\UniversityRepository;
use App\Http\Requests\Backend\Universities\ManageUniversityRequest;
use App\Http\Requests\Backend\Universities\CreateUniversityRequest;
use App\Http\Requests\Backend\Universities\StoreUniversityRequest;
use App\Http\Requests\Backend\Universities\EditUniversityRequest;
use App\Http\Requests\Backend\Universities\UpdateUniversityRequest;
use App\Http\Requests\Backend\Universities\DeleteUniversityRequest;

/**
 * UniversitiesController
 */
class UniversitiesController extends Controller
{
    /**
     * variable to store the repository object
     * @var UniversityRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param UniversityRepository $repository;
     */
    public function __construct(UniversityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Universities\ManageUniversityRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageUniversityRequest $request)
    {
        return new ViewResponse('backend.universities.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateUniversityRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Universities\CreateResponse
     */
    public function create(CreateUniversityRequest $request)
    {
        return new CreateResponse('backend.universities.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUniversityRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreUniversityRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.universities.index'), ['flash_success' => _tr('alerts.backend.universities.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Universities\University  $university
     * @param  EditUniversityRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Universities\EditResponse
     */
    public function edit(University $university, EditUniversityRequest $request)
    {
        return new EditResponse($university);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUniversityRequestNamespace  $request
     * @param  App\Models\Universities\University  $university
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateUniversityRequest $request, University $university)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $university, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.universities.index'), ['flash_success' => _tr('alerts.backend.universities.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteUniversityRequestNamespace  $request
     * @param  App\Models\Universities\University  $university
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(University $university, DeleteUniversityRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($university);
        //returning with successfull message
        return new RedirectResponse(route('admin.universities.index'), ['flash_success' => _tr('alerts.backend.universities.deleted')]);
    }
    
}
