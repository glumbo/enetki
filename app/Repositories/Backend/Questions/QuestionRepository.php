<?php

namespace App\Repositories\Backend\Questions;

use DB;
use Carbon\Carbon;
use App\Models\Questions\Question;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class QuestionRepository.
 */
class QuestionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Question::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.questions.table').'.id',
                config('module.questions.table').'.lesson_id',
                config('module.questions.table').'.video_id',
                config('module.questions.table').'.type',
                config('module.questions.table').'.content',
                config('module.questions.table').'.a',
                config('module.questions.table').'.b',
                config('module.questions.table').'.c',
                config('module.questions.table').'.d',
                config('module.questions.table').'.e',
                config('module.questions.table').'.right_answer',
                config('module.questions.table').'.created_at',
                config('module.questions.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Question::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.questions.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Question $question
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Question $question, array $input)
    {
    	if ($question->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.questions.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Question $question
     * @throws GeneralException
     * @return bool
     */
    public function delete(Question $question)
    {
        if ($question->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.questions.delete_error'));
    }
}
