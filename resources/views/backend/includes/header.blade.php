  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('frontend.index')}}" class="nav-link">{{trans('navs.general.home')}}</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('admin.contacts.index')}}" class="nav-link">{{trans('navs.general.contacts')}}</a>
      </li>
    </ul>

    {{-- @include('backend.includes.navbar.navbar_search') --}}

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
    {{--  @include('backend.includes.navbar.messages') --}}
    {{--  @include('backend.includes.navbar.notifications') --}}
      @include('backend.includes.navbar.user_menu')
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->