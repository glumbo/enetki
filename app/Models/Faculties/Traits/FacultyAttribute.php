<?php

namespace App\Models\Faculties\Traits;

/**
 * Class FacultyAttribute.
 */
trait FacultyAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return "<div class=\"btn-group action-btn\"> {$this->getEditButtonAttribute("edit-faculty", "admin.faculties.edit")}
                {$this->getDeleteButtonAttribute("delete-faculty", "admin.faculties.destroy")}
                </div>";
    }
}
