<?php

Breadcrumbs::register('admin.cards.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.cards.management'), route('admin.cards.index'));
});

Breadcrumbs::register('admin.cards.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.cards.index');
    $breadcrumbs->push(trans('menus.backend.cards.create'), route('admin.cards.create'));
});

Breadcrumbs::register('admin.cards.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cards.index');
    $breadcrumbs->push(trans('menus.backend.cards.edit'), route('admin.cards.edit', $id));
});
