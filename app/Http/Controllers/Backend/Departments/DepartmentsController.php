<?php

namespace App\Http\Controllers\Backend\Departments;

use App\Models\Departments\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Departments\CreateResponse;
use App\Http\Responses\Backend\Departments\EditResponse;
use App\Repositories\Backend\Departments\DepartmentRepository;
use App\Http\Requests\Backend\Departments\ManageDepartmentRequest;
use App\Http\Requests\Backend\Departments\CreateDepartmentRequest;
use App\Http\Requests\Backend\Departments\StoreDepartmentRequest;
use App\Http\Requests\Backend\Departments\EditDepartmentRequest;
use App\Http\Requests\Backend\Departments\UpdateDepartmentRequest;
use App\Http\Requests\Backend\Departments\DeleteDepartmentRequest;

/**
 * DepartmentsController
 */
class DepartmentsController extends Controller
{
    /**
     * variable to store the repository object
     * @var DepartmentRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param DepartmentRepository $repository;
     */
    public function __construct(DepartmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Departments\ManageDepartmentRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageDepartmentRequest $request)
    {
        return new ViewResponse('backend.departments.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateDepartmentRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Departments\CreateResponse
     */
    public function create(CreateDepartmentRequest $request)
    {
        return new CreateResponse('backend.departments.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDepartmentRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreDepartmentRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.departments.index'), ['flash_success' => _tr('alerts.backend.departments.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Departments\Department  $department
     * @param  EditDepartmentRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Departments\EditResponse
     */
    public function edit(Department $department, EditDepartmentRequest $request)
    {
        return new EditResponse($department);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateDepartmentRequestNamespace  $request
     * @param  App\Models\Departments\Department  $department
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateDepartmentRequest $request, Department $department)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $department, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.departments.index'), ['flash_success' => _tr('alerts.backend.departments.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteDepartmentRequestNamespace  $request
     * @param  App\Models\Departments\Department  $department
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Department $department, DeleteDepartmentRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($department);
        //returning with successfull message
        return new RedirectResponse(route('admin.departments.index'), ['flash_success' => _tr('alerts.backend.departments.deleted')]);
    }
    
}
