@extends('frontend.layouts.app')

@section('title'){{ $page->seo_title }}@endsection
@section('meta_description'){{ $page->seo_description }}@endsection
@section('meta_keywords'){{ $page->seo_keyword }}@endsection

@section('content')
<div id="scroller">
    <div id="feature" class="section">
        <div id="parallax" class="background" data-desktop="{{assets('theme/static/images/backgrounds/f9_feature.jpg')}}" data-mobile="{{assets('theme/static/images/backgrounds/f9_feature.jpg')}}"></div>
        <div id="opacity" class="section-inner feature">
            <div class="inner-center">
                <h1 class="animate">Falcon 9</h1>
                <h3 class="animate">First Orbital Class Rocket capable of reflight</h3>
            </div>
            <div id="scrollme">
                <svg width="30px" height="20px">
                    <path stroke="#ffffff" stroke-width="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 " />
                </svg>
            </div>
        </div>
    </div>
    <div id="stats" class="section">
        <div class="content-center">
            <div id="stat1" class="column3 stat">
                <span class="number">128</span>
                <span class="label">Total Launches</span>
            </div>
            <div id="stat2" class="column3 stat">
                <span class="number">88</span>
                <span class="label">Total Landings</span>
            </div>
            <div id="stat3" class="column3 stat">
                <span class="number">70</span>
                <span class="label">Reflown Rockets </span>
            </div>
        </div>
    </div>

    <div id="reveal" class="section resize">
        <div class="reveal-inner section-inner">
            <div class="inner-left-middle">
                <h4 class="animate">Falcon 9 is a reusable, two-stage rocket designed and manufactured by Enetki for the reliable and safe transport of people and payloads into Earth orbit and beyond. Falcon 9 is the world’s first orbital class reusable rocket. Reusability allows Enetki to refly the most expensive parts of the rocket, which in turn drives down the cost of space access.</h4>
            </div>
        </div>
    </div>

    <div id="slider" class="slider quickjump section swiper-container" data-vehicle>
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="background gallery-background gallery-full overflow-visible">
                    <div class="reveal-bg" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9Fairings_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9Fairings_Render_Mobile.jpg')}}"></div>
                    <div class="reveal-container">
                        <div class="reveal-overflow u-fullParent">
                            <div class="reveal-render u-fullParent" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9Fairings_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9Fairings_Render_Mobile.jpg')}}">
                                <video id="reveal-video" autoplay loop muted width="100%" height="100%" preload="auto" autobuffer="true">
                                    <source type="video/mp4" src="{{assets('theme/media/WebsiteF9Fairings_Anim_Render_Desktop.mp4')}}">
                                    <source type="video/webm" src="{{assets('theme/media/WebsiteF9Fairings_Anim_Render_Desktop.webm')}}">
                                </video>
                            </div>
                            <div id="reveal-lines" class="reveal-lines u-fullParent" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9Fairings_Lines_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9Fairings_Lines_Mobile.jpg')}}">
                                <video loop muted width="100%" height="100%" preload="auto" autobuffer="true">
                                    <source type="video/mp4" src="{{assets('theme/media/WebsiteF9Fairings_Anim_Lines_Desktop.mp4')}}">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-inner">
                    <div id="reveal-overview" class="inner-left-middle">
                        <h3 class="js-stagger">Falcon 9</h3>
                        <h2 class="js-stagger">Overview</h2>
                        <table class="data additional-toggle" style="display: table;">
                            <tbody>
                                <tr class="js-stagger">
                                    <td>HEIGHT</td>
                                    <td>70 m <span>/ 229.6 ft</span></td>
                                </tr>
                                <tr class="js-stagger">
                                    <td>DIAMETER</td>
                                    <td>3.7 m <span>/ 12 ft</span></td>
                                </tr>
                                <tr class="js-stagger">
                                    <td>MASS</td>
                                    <td>549,054 kg <span>/ 1,207,920 lb</span></td>
                                </tr>
                                <tr class="js-stagger">
                                    <td>PAYLOAD TO LEO</td>
                                    <td>22,800 kg <span>/ 50,265 lb</span></td>
                                </tr>
                                <tr class="js-stagger">
                                    <td>PAYLOAD TO GTO</td>
                                    <td>8,300 kg <span>/ 18,300 lb</span></td>
                                </tr>
                                <tr class="js-stagger">
                                    <td>PAYLOAD TO MARS</td>
                                    <td>4,020 kg <span>/ 8,860 lb</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="background gallery-background gallery-full">
                    <div class="background-tab" data-tab-bg="overview" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9S1_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9S1_Render_Mobile.jpg')}}"></div>
                    <div class="background-tab" hidden data-tab-bg="engines" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9Engines_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9Engines_Render_Mobile.jpg')}}"></div>
                    <div class="background-tab" hidden data-tab-bg="landing-legs" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9S1Legs_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9S1Legs_Render_Mobile.jpg')}}"></div>
                </div>
                <div class="section-inner">
                    <div class="inner-left-middle">
                        <h3 class="js-bounding">Falcon 9</h3>
                        <h2>First stage</h2>
                        <div class="content-tabs" role="tablist">
                            <button role="tab" data-tab-bg="overview" aria-selected="true" aria-controls="overview-tab-panel" id="overview-tab">Overview</button>|
                            <button role="tab" data-tab-bg="engines" aria-selected="false" aria-controls="engines-tab-panel" id="engines-tab" tabindex="-1">Engines</button>|
                            <button role="tab" data-tab-bg="landing-legs" aria-selected="false" aria-controls="landing-legs-tab-panel" id="landing-legs-tab" tabindex="-1">Landing legs</button>
                        </div>
                        <div class="content-tab-data" data-tab-bg="overview" tabindex="0" role="tabpanel" id="overview-tab-panel" aria-labelledby="overview-tab">
                            <p>Falcon 9’s first stage incorporates nine Merlin engines and aluminum-lithium alloy tanks containing liquid oxygen and rocket-grade kerosene (RP-1) propellant.</p>
                            <p>Falcon 9 generates more than 1.7 million pounds of thrust at sea level.</p>
                        </div>
                        <div class="content-tab-data" data-tab-bg="engines" tabindex="0" role="tabpanel" id="engines-tab-panel" aria-labelledby="engines-tab" hidden>
                            <p>The nine Merlin engines on the first stage are gradually throttled near the end of first-stage flight to limit launch vehicle acceleration as the rocket’s mass decreases with the burning of fuel. These engines are also used to reorient the first stage prior to reentry and to decelerate the vehicle for landing.</p>
                            <table class="data">
                                <tbody>
                                    <tr>
                                        <td>NUMBER OF ENGINES</td>
                                        <td>9</td>
                                    </tr>
                                    <tr>
                                        <td>THRUST AT SEA LEVEL</td>
                                        <td>7,607 kN <span>/ 1,710,000 lbf </span></td>
                                    </tr>
                                    <tr>
                                        <td>THRUST IN VACUUM</td>
                                        <td>8,227 kN <span>/ 1,849,500 lbf</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="content-tab-data" data-tab-bg="landing-legs" tabindex="0" role="tabpanel" id="landing-legs-tab-panel" aria-labelledby="landing-legs-tab" hidden>
                            <p>The Falcon 9 first stage is equipped with four landing legs made of state-of-the-art carbon fiber with aluminum honeycomb.</p>
                            <p>Placed symmetrically around the base of the rocket, they are stowed at the base of the vehicle and deploy just prior to landing.</p>
                            <p>
                                <a href="../../mission/index.html" class="hover-none">
                                    <strong>Learn more about Enetki reusability</strong>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.27" height="10.154" viewbox="0 0 6.27 10.154">
                                        <path d="M8.59,14.711l3.876-3.884L8.59,6.943,9.783,5.75l5.077,5.077L9.783,15.9Z" transform="translate(-8.59 -5.75)" fill="#fff" />
                                    </svg>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="background gallery-background gallery-full" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9S2_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9S2_Render_Mobile.jpg')}}"></div>
                <div class="section-inner">
                    <div class="inner-left-middle">
                        <h3>Falcon 9</h3>
                        <h2>Second stage</h2>
                        <p>The second stage, powered by a single Merlin Vacuum Engine, delivers Falcon 9’s payload to the desired orbit. The second stage engine ignites a few seconds after stage separation, and can be restarted multiple times to place multiple payloads into different orbits. </p>
                        <table class="data">
                            <tbody>
                                <tr>
                                    <td>NUMBER OF ENGINES</td>
                                    <td>1 vacuum</td>
                                </tr>
                                <tr>
                                    <td>BURN TIME</td>
                                    <td>397 sec</td>
                                </tr>
                                <tr>
                                    <td>THRUST</td>
                                    <td>981 kN <span>/ 220,500 lbf</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="background gallery-background gallery-full" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9Interstage_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9Interstage_Render_Mobile.jpg')}}"></div>
                <div class="section-inner">
                    <div class="inner-left-middle">
                        <h3>Falcon 9</h3>
                        <h2>Interstage</h2>
                        <p>The interstage is a composite structure that connects the first and second stages, and houses the pneumatic pushers that allow the first and second stage to separate during flight.</p>
                        <h3>Grid fins</h3>
                        <p>Falcon 9 is equipped with four hypersonic grid fins positioned at the base of the interstage. They orient the rocket during reentry by moving the center of pressure.</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="background gallery-background gallery-full">
                    <div class="background-tab" data-tab-bg="fairing" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9SoloFairings_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9SoloFairings_Render_Mobile.jpg')}}"></div>
                    <div class="background-tab" hidden data-tab-bg="dragon" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/WebsiteF9SoloDragon_Render_Desktop.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/WebsiteF9SoloDragon_Render_Mobile.jpg')}}"></div>
                </div>
                <div class="section-inner">
                    <div class="inner-left-middle">
                        <h3>Falcon 9</h3>
                        <h2>Payload</h2>
                        <div class="content-tabs" role="tablist">
                            <button role="tab" data-tab-bg="fairing" aria-selected="true" aria-controls="fairing-tab-panel" id="fairing-tab">Fairing</button>|
                            <button role="tab" data-tab-bg="dragon" aria-selected="false" aria-controls="dragon-tab-panel" id="dragon-tab">Dragon</button>
                        </div>
                        <div class="content-tab-data" data-tab-bg="fairing" tabindex="0" role="tabpanel" id="fairing-tab-panel" aria-labelledby="fairing-tab">
                            <p>Made of a carbon composite material, the fairing protects satellites on their way to orbit. The fairing is jettisoned approximately 3 minutes into flight, and Enetki continues to recover fairings for reuse on future missions.</p>
                            <table class="data">
                                <tbody>
                                    <tr>
                                        <td>HEIGHT</td>
                                        <td>13.1 m <span>/ 43 ft</span></td>
                                    </tr>
                                    <tr>
                                        <td>DIAMETER</td>
                                        <td>5.2 m <span>/ 17.1 ft</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="content-tab-data" data-tab-bg="dragon" tabindex="0" role="tabpanel" id="dragon-tab-panel" aria-labelledby="dragon-tab" hidden>
                            <p>Dragon is capable of carrying up to 7 people and/or cargo in the spacecraft’s pressurized section. In addition, Dragon can carry cargo in the spacecraft’s unpressurized trunk, which can also accommodate secondary payloads.</p>
                            <p>
                                <a href="../dragon/index.html" class="hover-none">
                                    <strong>Learn more about Dragon</strong>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.27" height="10.154" viewbox="0 0 6.27 10.154">
                                        <path d="M8.59,14.711l3.876-3.884L8.59,6.943,9.783,5.75l5.077,5.077L9.783,15.9Z" transform="translate(-8.59 -5.75)" fill="#fff" />
                                    </svg>
                                </a>
                            </p>
                            <table class="data">
                                <tbody>
                                    <tr>
                                        <td>HEIGHT</td>
                                        <td>8.1 m <span>/ 26.6 ft</span></td>
                                    </tr>
                                    <tr>
                                        <td>DIAMETER</td>
                                        <td>3.7 m <span>/ 12 ft</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="controls">
            <div class="swiper-dots not-mobile"></div>
            <div id="gallery-prev-s" class="gallery-prev swiper-button-prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
                    <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(6) rotate(90)" fill="#fff" />
                </svg>
            </div>
            <div id="gallery-next-s" class="gallery-next swiper-button-next">
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
                    <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(0 10) rotate(-90)" fill="#fff" />
                </svg>
            </div>
        </div>
    </div>

    <div id="video" class="section" data-video="Z4TXCZG_NEY" data-title="Falcon 9 In Flight">
        <div class="background" data-preload data-desktop="{{assets('theme/static/images/backgrounds/f9_video.jpg')}}" data-mobile="{{assets('theme/static/images/backgrounds/f9_video.jpg')}}">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64.025" height="81.486" viewbox="0 0 64.025 81.486" class="play-arrow">
                <g>
                    <path d="M8,5V86.486L72.025,45.743Z" transform="translate(-8 -5)" fill="#fff" />
                </g>
            </svg>
        </div>
        <div class="section-inner resize">
            <div class="inner-left-bottom">
                <h3 class="animate">Video</h3>
                <h2 class="animate">Falcon 9 In Flight</h2>
            </div>
        </div>
    </div>
    <div id="engines" class="section collapse" data-taber>
        <div class="background gallery-background gallery-full">
            <div class="background-tab" data-tab-bg="sea-level" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/Merlin.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/Merlin.jpg')}}"></div>
            <div class="background-tab" hidden data-tab-bg="vacuum" data-preload data-desktop="{{assets('theme/static/images/falcon-9/desktop/MerlinVac.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/mobile/MerlinVac.jpg')}}"></div>
        </div>
        <div class="section-inner resize">
            <div class="inner-left-top">
                <h3 class="animate">Engines</h3>
                <h2 class="animate">Merlin</h2>
                <div class="content-tabs animate" role="tablist">
                    <button role="tab" data-tab-bg="sea-level" aria-selected="true" aria-controls="sea-level-tab-panel" id="sea-level-tab">SEA LEVEL</button>|
                    <button role="tab" data-tab-bg="vacuum" aria-selected="false" aria-controls="vacuum-tab-panel" id="vacuum-tab" tabindex="-1">VACUUM</button>
                </div>
                <div class="content-tab-data" data-tab-bg="sea-level" tabindex="0" role="tabpanel" id="sea-level-tab-panel" aria-labelledby="sea-level-tab">
                    <p class="animate">Merlin is a family of rocket engines developed by Enetki for use on its Falcon 1, Falcon 9 and Falcon Heavy launch vehicles. Merlin engines use a rocket grade kerosene (RP-1) and liquid oxygen as rocket propellants in a gas-generator power cycle. The Merlin engine was originally designed for recovery and reuse.</p>
                    <table class="data animate">
                        <tbody>
                            <tr>
                                <td>PROPELLANT</td>
                                <td>LOX <span>/ RP-1</span></td>
                            </tr>
                            <tr>
                                <td>THRUST</td>
                                <td>845 kN <span>/ 190,000 lbf</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="content-tab-data" data-tab-bg="vacuum" tabindex="0" role="tabpanel" id="vacuum-tab-panel" aria-labelledby="vacuum-tab" hidden>
                    <p class="animate">Merlin Vacuum features a larger exhaust section and a significantly larger expansion nozzle to maximize the engine’s efficiency in the vacuum of space. Its combustion chamber is regeneratively cooled, while the expansion nozzle is radiatively cooled. At full power, the Merlin Vacuum engine operates with the greatest efficiency ever for an American-made hydrocarbon rocket engine.</p>
                    <table class="data animate">
                        <tbody>
                            <tr>
                                <td>PROPELLANT</td>
                                <td>LOX <span>/ RP-1</span></td>
                            </tr>
                            <tr>
                                <td>THRUST</td>
                                <td>981 kN <span>/ 220,500 lbf</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="gallery" class="quickjump section swiper-container resize">
        <div class="swiper-wrapper resize">
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" style="background-position: center top 30%" data-preload data-desktop="{{assets('theme/static/images/falcon-9/refresh/F9_DM2_LAUNCH_3840x2560.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/refresh/F9_DM2_LAUNCH_800x1200_Mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 launches Dragon to the International Space Station from Launch Complex 39A</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_1.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_1_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 first and second stages after separating in flight</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_2.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_2_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 lifts off with its Iridium-5 payload</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_3.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_3_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 lands on the droneship Just Read the Instructions</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_4.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_4_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Close-up of Falcon 9's Merlin engines during liftoff</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_5.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_5_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 leaves a trail of light as it lifts off from Vandenberg Air Force Base</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_6.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_6_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 lifts off with Dragon for an in-flight test of the Crew Dragon abort system</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_7.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_7_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 lifts off with its Iridium-8 payload</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_8.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_8_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 with its Radarsat payload at sunset before launch</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_9.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_9_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 lands at Cape Canaveral</div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="gallery-background resize-gallery" data-preload data-desktop="{{assets('theme/static/images/falcon-9/F9_10.jpg')}}" data-mobile="{{assets('theme/static/images/falcon-9/F9_10_mobile.jpg')}}"></div>
                <div class="caption">
                    <div class="description">Falcon 9 vertical with its Iridium payload at moonrise</div>
                </div>
            </div>
        </div>
        <div id="gallery-prev-g" class="gallery-prev swiper-button-prev">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
                <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(6) rotate(90)" fill="#fff" />
            </svg>
        </div>
        <div id="gallery-next-g" class="gallery-next swiper-button-next">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
                <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(0 10) rotate(-90)" fill="#fff" />
            </svg>
        </div>
    </div>

    <div class="section">
        <div class="section-content">
            <div class="content-center">
                <div class="line-tall animate"></div>
                <p class="section-footer-a animate">For information about our launch services, contact <a href="mailto:sales@enetki.com">sales@enetki.com</a></p>
                <a class="btn special animate" tabindex="0" href="{{assets('theme/media/falcon-users-guide-2021-09.pdf')}}" target="_blank">
                    <div class="hover"></div>
                    <div class="text">Download User's Guide</div>
                </a>
                <a class="btn special animate" tabindex="0" href="{{assets('theme/media/Capabilities%26Services.pdf')}}" target="_blank">
                    <div class="hover"></div>
                    <div class="text">Capabilities and Services</div>
                </a>
            </div>
        </div>
    </div>


    @include('frontend.includes.footer')
</div>

@endsection