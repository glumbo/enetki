@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.members.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.members.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.members.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.members.partials.members-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="members-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.members.table.id') }}</th>
                            <th>{{ trans('labels.backend.members.table.name') }}</th>
                            <th>{{ trans('labels.backend.members.table.title') }}</th>
                            <th>{{ trans('labels.backend.members.table.cover') }}</th>
                            <th>{{ trans('labels.backend.members.table.social_media') }}</th>
                            <th>{{ trans('labels.backend.members.table.status') }}</th>
                            <th>{{ trans('labels.backend.members.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    @include('includes.datatables')

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#members-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.members.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.members.table')}}.id'},
                    {data: 'name', name: '{{config('module.members.table')}}.name'},
                    {data: 'title', name: '{{config('module.members.table')}}.title'},
                    {data: 'cover', name: '{{config('module.members.table')}}.cover'},
                    {data: 'social_media', name: '{{config('module.members.table')}}.facebook'},
                    {data: 'status', name: '{{config('module.members.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.members.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
