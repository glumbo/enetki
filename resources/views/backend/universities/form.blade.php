<div class="box-body">
    <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">
        {{ Form::label('city_id', _tr('labels.backend.cities.table.country'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('city_id', $cities, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.cities.table.country'), 'required' => 'required']) }}
           @if ($errors->has('city_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('city_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('short_name', _tr('labels.backend.universities.table.short_name'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('short_name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.universities.table.short_name')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('name', _tr('labels.backend.universities.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.universities.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
        {{ Form::label('type', _tr('labels.backend.universities.table.type'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('type', [0=>_tr('labels.backend.universities.table.private'),1=>_tr('labels.backend.universities.table.public')], null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.universities.table.type')]) }}
           @if ($errors->has('type'))
                <span class="help-block">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
