<?php

namespace App\Http\Controllers\Backend\QuizQuestion;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\QuizQuestion\QuizQuestionRepository;
use App\Http\Requests\Backend\QuizQuestion\ManageQuizQuestionRequest;

/**
 * Class QuizQuestionsTableController.
 */
class QuizQuestionsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var QuizQuestionRepository
     */
    protected $quizquestion;

    /**
     * contructor to initialize repository object
     * @param QuizQuestionRepository $quizquestion;
     */
    public function __construct(QuizQuestionRepository $quizquestion)
    {
        $this->quizquestion = $quizquestion;
    }

    /**
     * This method return the data of the model
     * @param ManageQuizQuestionRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageQuizQuestionRequest $request)
    {
        return Datatables::of($this->quizquestion->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('quiz', function ($quizquestion) {
                return $quizquestion->quiz?$quizquestion->quiz->name:'';
            })
            ->addColumn('question', function ($quizquestion) {
                return $quizquestion->question?$quizquestion->question->content:'';
            })
            ->addColumn('created_at', function ($quizquestion) {
                return Carbon::parse($quizquestion->created_at)->toDateString();
            })
            ->addColumn('actions', function ($quizquestion) {
                return $quizquestion->action_buttons;
            })
            ->make(true);
    }
}
