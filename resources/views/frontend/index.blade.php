@extends('frontend.layouts.app')

@section('content')
@if($sliders)
@foreach ($sliders as $key => $slider)
<div {{ $key===0 ? 'id=feature' : '' }} class="section">
    <div class="background" data-preload data-desktop="{{ Storage::disk('public')->url($slider->cover) }}" data-mobile="{{ Storage::disk('public')->url($slider->cover) }}" style="background-image: url({{ Storage::disk('public')->url($slider->cover) }}); visibility: inherit; opacity: 1;"></div>
    <div class="section-inner feature">
        <div class="inner-left-bottom">
            <h4 class="animate" style="text-transform: uppercase">{{ $slider->subtitle }}</h4>
            @if($key === 0)
            <h1 class="animate shadowed">{{ $slider->title }}</h2>
                @else
                <h2 class="animate shadowed">{{ $slider->title }}</h2>
                @endif
                <a class="btn animate" tabindex="0" href="{{ $slider->button1_link }}">
                    <div class="hover"></div>
                    <span class="text">{{ $slider->button1 }}</span>
                </a>
        </div>
        <div id="scrollme">
            <svg width="30px" height="20px">
                <path stroke="#ffffff" stroke-width="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 " />
            </svg>
        </div>
    </div>
</div>
@endforeach
@endif

@include('frontend.includes.footer')
@endsection