<?php

namespace App\Models\Questions\Traits;
use App\Models\Videos\Video;
use App\Models\Lessons\Lesson;

/**
 * Class QuestionRelationship
 */
trait QuestionRelationship
{
    public function video()
    {
        return $this->belongsTo(Video::class,'video_id');
    }
    public function lesson()
    {
        return $this->belongsTo(Lesson::class,'lesson_id');
    }
}
