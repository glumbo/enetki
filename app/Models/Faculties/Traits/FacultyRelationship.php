<?php

namespace App\Models\Faculties\Traits;
use App\Models\Universities\University;
/**
 * Class FacultyRelationship
 */
trait FacultyRelationship
{
    public function university()
    {
        return $this->belongsTo(University::class,'university_id');
    }
}
