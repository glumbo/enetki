<?php

namespace App\Models\Universities\Traits;
use App\Models\Cities\City;

/**
 * Class UniversityRelationship
 */
trait UniversityRelationship
{
    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }
}
