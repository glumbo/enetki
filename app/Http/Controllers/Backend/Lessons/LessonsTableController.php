<?php

namespace App\Http\Controllers\Backend\Lessons;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Lessons\LessonRepository;
use App\Http\Requests\Backend\Lessons\ManageLessonRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\Lessons\Lesson;

/**
 * Class LessonsTableController.
 */
class LessonsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var LessonRepository
     */
    protected $lesson;

    /**
     * contructor to initialize repository object
     * @param LessonRepository $lesson;
     */
    public function __construct(LessonRepository $lesson)
    {
        $this->lesson = $lesson;
    }

    /**
     * This method return the data of the model
     * @param ManageLessonRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageLessonRequest $request)
    {
        return Datatables::of($this->lesson->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('department', function ($lesson) {
                return $lesson->department?$lesson->department->name:'';
            })
            ->addColumn('teacher', function ($lesson) {
                return $lesson->teacher?$lesson->teacher->title.' '.$lesson->teacher->first_name.' '.$lesson->teacher->last_name:'';
            })
            ->addColumn('parent', function ($lesson) {
                return $lesson->parent?'<a href="'.route("admin.lessons.show",$lesson->parent->id).'">'.$lesson->parent->name.'</a>':'';
            })
            ->addColumn('video', function ($lesson) {
                return $lesson->video?$lesson->video->name:'';
            })
            ->addColumn('cover', function ($lesson) {
                return $lesson->cover?'<img height="50" width="50" src="'.Storage::disk('public')->url($lesson->cover).'">':'';
            })
            ->addColumn('name', function ($lesson) {
                return $lesson->children->count()>0?'<a href="'.route("admin.lessons.show",$lesson->id).'">'.$lesson->name.'</a>':$lesson->name;
            })
            ->addColumn('price', function ($lesson) {
                return $lesson->price?$lesson->price.' ₺':'';
            })
            ->addColumn('created_at', function ($lesson) {
                return Carbon::parse($lesson->created_at)->toDateString();
            })
            ->addColumn('actions', function ($lesson) {
                return $lesson->action_buttons;
            })
            ->make(true);
    }

    public function parent(Lesson $lesson){
        return Datatables::of($this->lesson->getForDataTable()->where('parent_id',$lesson->id))
        ->escapeColumns(['id'])
        ->addColumn('department', function ($lesson) {
            return $lesson->department?$lesson->department->name:'';
        })
        ->addColumn('teacher', function ($lesson) {
            return $lesson->teacher?$lesson->teacher->title.' '.$lesson->teacher->first_name.' '.$lesson->teacher->last_name:'';
        })
        ->addColumn('parent', function ($lesson) {
            return $lesson->parent?$lesson->parent->name:'';
        })
        ->addColumn('video', function ($lesson) {
            return $lesson->video?$lesson->video->name:'';
        })
        ->addColumn('cover', function ($lesson) {
            return $lesson->cover?'<img height="50" width="50" src="'.Storage::disk('public')->url($lesson->cover).'">':'';
        })
        ->addColumn('name', function ($lesson) {
            return $lesson->children->count()>0?'<a href="'.route("admin.lessons.show",$lesson->id).'">'.$lesson->name.'</a>':$lesson->name;
        })
        ->addColumn('created_at', function ($lesson) {
            return Carbon::parse($lesson->created_at)->toDateString();
        })
        ->addColumn('actions', function ($lesson) {
            return $lesson->action_buttons;
        })
        ->make(true);
    }
}
