@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.departments.management') . ' | ' . _tr('labels.backend.departments.edit'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.departments.management') }}
        <small>{{ _tr('labels.backend.departments.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($departments, ['route' => ['admin.departments.update', $departments], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-department']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.departments.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.departments.partials.departments-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.departments.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.departments.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
