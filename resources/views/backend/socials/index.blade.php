@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.socials.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.socials.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.socials.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.socials.partials.socials-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="socials-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.socials.table.id') }}</th>
                            <th>{{ _tr('labels.backend.socials.table.type') }}</th>
                            <th>{{ _tr('labels.backend.socials.table.url') }}</th>
                            <th>{{ _tr('labels.backend.socials.table.status') }}</th>
                            <th>{{ _tr('labels.backend.socials.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#socials-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.socials.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.socials.table')}}.id'},
                    {data: 'type', name: '{{config('module.socials.table')}}.type'},
                    {data: 'url', name: '{{config('module.socials.table')}}.url'},
                    {data: 'status', name: '{{config('module.socials.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.socials.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
