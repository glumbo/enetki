<?php

namespace App\Repositories\Backend\Faculties;

use DB;
use Carbon\Carbon;
use App\Models\Faculties\Faculty;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FacultyRepository.
 */
class FacultyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Faculty::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.faculties.table').'.id',
                config('module.faculties.table').'.university_id',
                config('module.faculties.table').'.name',
                config('module.faculties.table').'.created_at',
                config('module.faculties.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Faculty::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.faculties.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Faculty $faculty
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Faculty $faculty, array $input)
    {
    	if ($faculty->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.faculties.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Faculty $faculty
     * @throws GeneralException
     * @return bool
     */
    public function delete(Faculty $faculty)
    {
        if ($faculty->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.faculties.delete_error'));
    }
}
