<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', app_name())</title>
  <meta name="description" content="@yield('meta_description', 'Default Description')">
  <meta name="author" content="@yield('meta_author', 'Glumbo')">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="app_url" id="app_url" content="{{ asset('') }}">
  {{-- <link rel="icon" sizes="16x16" type="image/png" href="{{route('frontend.index')}}/img/favicon_icon/{{settings()->favicon}}"> --}}
  @yield('meta')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  @yield('before-styles')
  <!-- Font Awesome -->
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
  <link rel="stylesheet" href="{{assets('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{assets('plugins/font-awesome/css/all.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{assets('dist/css/adminlte.css')}}">

  <link rel="stylesheet" href="{{assets('dist/css/custom.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @langrtl
      {{ Html::style('public'.getRtlCss(mix('css/backend.css'))) }}
  @else
      {{ Html::style('public'.mix('css/backend.css')) }}
  @endlangrtl

  {{ Html::style('public'.mix('css/backend-custom.css')) }}

  <link rel="stylesheet" href="{{assets('css/custom-style.css')}}">
  @yield('after-styles')

  @toastr_css

  <script>
      window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!};
  </script>
  <?php
      if (!empty($google_analytics)) {
          echo $google_analytics;
      }
  ?>
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<!-- Site wrapper -->
<div class="wrapper">
    @include('backend.includes.header')
    @include('backend.includes.sidebar-dynamic')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          @yield('page-header')
          </div>
          <div class="col-sm-6">
          @if(Breadcrumbs::exists())
              {!! Breadcrumbs::render() !!}
          @endif
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            @include('includes.partials.messages')
            @yield('content')
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('backend.includes.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@yield('before-scripts')
<!-- jQuery -->
<script src="{{assets('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{assets('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{assets('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{assets('dist/js/demo.js')}}"></script>

{{ Html::script('public'.mix('js/backend.js')) }}
{{ Html::script('public'.mix('js/backend-custom.js')) }}
@yield('after-scripts')

@toastr_js
@toastr_render
</body>
</html>
