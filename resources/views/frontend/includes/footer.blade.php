<div id="footer">
  <p>
    <span>Enetki &copy; 2021</span>
    {{ renderMenuItems(getMenuItems('footer') , 'frontend.includes.partials.menu-item', 'footer') }}
  </p>
</div>