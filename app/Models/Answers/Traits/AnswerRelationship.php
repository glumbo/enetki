<?php

namespace App\Models\Answers\Traits;
use App\Models\Quizzes\Quiz;
use App\Models\Questions\Question;
use App\Models\Access\User\User;

/**
 * Class AnswerRelationship
 */
trait AnswerRelationship
{

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function question() {
        return $this->belongsTo(Question::class, 'question_id');
    }
    public function quiz() {
        return $this->belongsTo(Quiz::class, 'quiz_id');
    }
}
