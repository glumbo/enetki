<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\Navbars\Navbar;
use App\Observers\PlatformObserver;
use App\Models\Settings\Setting;
use Event;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        /*
         * Application locale defaults for various components
         *
         * These will be overridden by LocaleMiddleware if the session local is set
         */
        Event::listen('eloquent.creating:*',PlatformObserver::class);

        /*
         * setLocale for php. Enables ->formatLocalized() with localized values for dates
         */
        setlocale(LC_TIME, config('app.locale_php'));

        /*
         * setLocale to use Carbon source locales. Enables diffForHumans() localized
         */
        Carbon::setLocale(config('app.locale'));

        /*
         * Set the session variable for whether or not the app is using RTL support
         * For use in the blade directive in BladeServiceProvider
         */
        if (config('locale.languages')[config('app.locale')][2]) {
            session(['lang-rtl' => true]);
        } else {
            session()->forget('lang-rtl');
        }

        // Force SSL in production
        if ($this->app->environment() == 'production') {
            //URL::forceScheme('https');
        }

        // Set the default string length for Laravel5.4
        // https://laravel-news.com/laravel-5-4-key-too-long-error
        Schema::defaultStringLength(191);

        view()->composer(['frontend.*'], function ($view) {
            $setting = Setting::first();
            $navbars = Navbar::where(['parent_id' => null, 'status' => 1, 'is_spesific' => 0])->get();
            $spesific_navbar = Navbar::where(['parent_id' => null, 'status' => 1, 'is_spesific' => 1])->first();
            $view->with(['setting' => $setting, 'navbars'=>  $navbars ,'spesific_navbar' => $spesific_navbar]);
        });

    }

    /**
     * Register any application services.
     */
    public function register()
    {
        /*
         * Sets third party service providers that are only needed on local/testing environments
         */
        if ($this->app->environment() != 'production') {
        }
    }
}
