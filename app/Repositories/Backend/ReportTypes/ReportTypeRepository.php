<?php

namespace App\Repositories\Backend\ReportTypes;

use DB;
use Carbon\Carbon;
use App\Models\ReportTypes\ReportType;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportTypeRepository.
 */
class ReportTypeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ReportType::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.reporttypes.table').'.id',
                config('module.reporttypes.table').'.parent_id',
                config('module.reporttypes.table').'.name',
                config('module.reporttypes.table').'.created_at',
                config('module.reporttypes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (ReportType::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.reporttypes.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param ReportType $reporttype
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(ReportType $reporttype, array $input)
    {
    	if ($reporttype->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.reporttypes.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param ReportType $reporttype
     * @throws GeneralException
     * @return bool
     */
    public function delete(ReportType $reporttype)
    {
        if ($reporttype->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.reporttypes.delete_error'));
    }
}
