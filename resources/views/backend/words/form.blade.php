<div class="box-body">
    <div class="form-group {{ $errors->has('lang') ? ' has-error' : '' }}">
        {{ Form::label('lang', _tr('labels.backend.words.table.lang'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('lang', $langs, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.words.table.lang'), 'required' => 'required']) }}
           @if ($errors->has('lang'))
                <span class="help-block">
                    <strong>{{ $errors->first('lang') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('word', _tr('labels.backend.words.table.word'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('word', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.words.table.word'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('meaning', _tr('labels.backend.words.table.meaning'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('meaning', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.words.table.meaning'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script lang="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
