<?php

namespace App\Repositories\Backend\Quizzes;

use DB;
use Carbon\Carbon;
use App\Models\Quizzes\Quiz;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class QuizRepository.
 */
class QuizRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Quiz::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.quizzes.table').'.id',
                config('module.quizzes.table').'.name',
                config('module.quizzes.table').'.created_at',
                config('module.quizzes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Quiz::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.quizzes.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Quiz $quiz
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Quiz $quiz, array $input)
    {
    	if ($quiz->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.quizzes.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Quiz $quiz
     * @throws GeneralException
     * @return bool
     */
    public function delete(Quiz $quiz)
    {
        if ($quiz->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.quizzes.delete_error'));
    }
}
