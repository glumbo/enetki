<li class="nav-item dropdown user-menu">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <img src="{{assets('dist/img/user-icon.png')}}" class="user-image img-circle elevation-2" alt="User Image">
        <span class="d-none d-md-inline">{{$logged_in_user->name}}</span>
    </a>
    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
        <!-- User image -->
        <li class="user-header bg-primary">
            <!-- <img src="{{assets('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image"> -->

            <p>

                {{$logged_in_user->name}} - {{$logged_in_user->roles()->first()->name}}
                <small>Member since {{$logged_in_user->created_at->format('M d, Y')}}</small>
            </p>
        </li>
        <li class="user-body">
            <div class="col-xs-12 text-center">
              <a href="{{route('admin.profile.edit')}}" ><i class="fal fa-user"></i> {{ trans('navs.general.edit_profile') }}</a>
            </div>
        </li>
        <li class="user-body border-left">
            <div class="col-xs-12 text-center">
              <a href="{{route('admin.access.user.change-password',access()->user()->id)}}" >{{ trans('navs.general.change_password') }}</a>
            </div>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <a href="{{route('frontend.index')}}" class="col-5 inline-block btn btn-default btn-flat"><i class="fal fa-home"></i> {{ trans('navs.general.home') }}</a>
            <a href="{{route('frontend.auth.logout')}}" class="col-5 inlnie-block btn btn-default btn-flat float-right"><i class="fal fa-sign-out"></i> {{ trans('navs.general.logout') }}</a>
        </li>
    </ul>
</li>