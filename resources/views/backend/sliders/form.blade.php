<div class="box-body">
    <div class="form-group">
        {{ Form::label('cover', _tr('labels.backend.sliders.table.cover'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($sliders->cover))
            <div class="col-lg-1 file_show">
                <img src="{{ Storage::disk('public')->url($sliders->cover) }}" height="80" width="80">
                <i class="fal fa-times remove_file"></i>
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                    <input type="hidden" name="remove_file" id="remove_file" value="0" />
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('subtitle', _tr('labels.backend.sliders.table.subtitle'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('subtitle', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.subtitle')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('title', _tr('labels.backend.sliders.table.title'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.title')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('description', _tr('labels.backend.sliders.table.description'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('description', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.description')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('button1', _tr('labels.backend.sliders.table.button1'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('button1', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.button1')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('button2', _tr('labels.backend.sliders.table.button2'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('button2', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.button2')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('button1_link', _tr('labels.backend.sliders.table.button1_link'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('button1_link', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.button1_link')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('button2_link', _tr('labels.backend.sliders.table.button2_link'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('button2_link', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.sliders.table.button2_link')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('status', _tr('labels.backend.sliders.table.status'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.sliders.table.status')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
