<?php

namespace App\Repositories\Backend\QuizQuestion;

use DB;
use Carbon\Carbon;
use App\Models\QuizQuestion\QuizQuestion;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class QuizQuestionRepository.
 */
class QuizQuestionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = QuizQuestion::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.quizquestions.table').'.id',
                config('module.quizquestions.table').'.quiz_id',
                config('module.quizquestions.table').'.question_id',
                config('module.quizquestions.table').'.order',
                config('module.quizquestions.table').'.created_at',
                config('module.quizquestions.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (QuizQuestion::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.quizquestions.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param QuizQuestion $quizquestion
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(QuizQuestion $quizquestion, array $input)
    {
    	if ($quizquestion->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.quizquestions.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param QuizQuestion $quizquestion
     * @throws GeneralException
     * @return bool
     */
    public function delete(QuizQuestion $quizquestion)
    {
        if ($quizquestion->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.quizquestions.delete_error'));
    }
}
