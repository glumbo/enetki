
	@extends('frontend.layouts.app')
	@section('content')
	<div id="feature" class="section">
				<div id="parallax" class="background" data-desktop="{{assets('theme/static/images/backgrounds/mission_feature.jpg')}}" data-mobile="{{assets('theme/static/images/backgrounds/mission_feature_mobile.jpg')}}"></div>
				<div id="opacity" class="section-inner feature">
					<div class="inner-center quote">
						<h1 class="animate">“You want to wake up in the morning and think the future is going to be great - and that’s what being a spacefaring civilization is all about. It’s about believing in the future and thinking that the future will be better than the past. And I can’t think of anything more exciting than going out there and being among the stars.”</h1>
						<p class="author animate">-Elon Musk</p>
					</div>
					<div id="scrollme">
						<svg width="30px" height="20px">
							<path stroke="#ffffff" stroke-width="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 "/>
						</svg>
					</div>
				</div>
			</div>

			<div id="stats" style="width: 100%; text-align: center; margin: 0; padding: 0;">
				<div class="content-center">
					<div id="stat1" class="column3 stat animate">
						<span class="number">133</span> <!-- these variables can be found in ./mission.json! -->
						<span class="label">Total Launches</span>
					</div>
					<div id="stat2" class="column3 stat animate">
						<span class="number">95</span> <!-- these variables can be found in ./mission.json! -->
						<span class="label">Total Landings</span>
					</div>
					<div id="stat3" class="column3 stat animate">
						<span class="number">74</span>  <!-- these variables can be found in ./mission.json! -->
						<span class="label">Reflown Rockets </span>
					</div>
				</div>
			</div>

			<div id="humans" class="section">
				<div class="background" data-preload data-desktop="{{assets('theme/static/images/backgrounds/mission_humans.jpg')}}" data-mobile="{{assets('theme/static/images/backgrounds/mission_humans-mobile.jpg')}}"></div>
				<div class="section-inner resize">
					<div class="inner-left-top">
						<h2 class="animate">MAKING HUMANITY MULTIPLANETARY</h2>
						<p class="animate">Building on the achievements of Falcon 9 and Falcon Heavy, enetki is working on a next generation of fully reusable launch vehicles that will be the most powerful ever built, capable of carrying humans to Mars and other destinations in the solar system.</p>
						<a class="btn animate" tabindex="0" href="../vehicles/starship/index.html">
							<div class="hover"></div>
							<div class="text">Visit Starship</div>
						</a>
					</div>
				</div>
			</div>

			<div id="slider" class="slider quickjump section swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-01.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-01.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h2 class="animate">MAKING HISTORY</h2>
								<p class="animate">enetki has gained worldwide attention for a series of historic milestones. It is the only private company capable of returning a spacecraft from low-Earth orbit, and in 2012 our Dragon spacecraft became the first commercial spacecraft to deliver cargo to and from the International Space Station. And in 2020, enetki became the first private company to take humans there as well. Click through the timeline above to see some of our milestone accomplishments.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-08.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-08.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate js-bounding">SEPTEMBER 2008</h3>
								<h2 class="animate">FALCON 1 MAKES HISTORY</h2>
								<p class="animate">Falcon 1 becomes the first privately developed liquid fuel rocket to reach Earth orbit.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-07.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-07.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">APRIL 2012</h3>
								<h2 class="animate">DRAGON REACHES THE SPACE STATION</h2>
								<p class="animate">Dragon becomes the first private spacecraft in history to visit the space station.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-06.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-06.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">DECEMBER 2015</h3>
								<h2 class="animate">FIRST LAND LANDING</h2>
								<p class="animate">On December 21, 2015, the Falcon 9 rocket delivered 11 communications satellites to orbit, and the first stage returned and landed at Landing Zone 1 — the first-ever orbital class rocket landing.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-05.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-05.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">APRIL 2016</h3>
								<h2 class="animate">DRONESHIP LANDING</h2>
								<p class="animate">On April 8, 2016, the Falcon 9 rocket launched the Dragon spacecraft to the International Space Station, and the first stage returned and landed on the “Of Course I Still Love You” droneship.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-04.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-04.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">MARCH 2017</h3>
								<h2 class="animate">FIRST REFLIGHT</h2>
								<p class="animate">On March 30, 2017, enetki achieved the world’s first reflight of an orbital class rocket. Following delivery of the payload, the Falcon 9 first stage returned to Earth for the second time.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-03.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-03.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">FEBRUARY 2018</h3>
								<h2 class="animate">FALCON HEAVY FIRST FLIGHT</h2>
								<p class="animate">Falcon Heavy is the world’s most powerful operational rocket by a factor of two, capable of carrying large payloads to orbit and supporting missions as far as the Moon or Mars. On February 7, 2018, Falcon Heavy made its first launch to orbit, successfully landing 2 of its 3 boosters and launching its payload to space. </p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/mission/mission-slider-02.jpg')}}" data-mobile="{{assets('theme/static/images/mission/mission-slider-02.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">MARCH 2019</h3>
								<h2 class="animate">DRAGON DOCKS WITH ISS</h2>
								<p class="animate">Dragon docked with the International Space Station on March 3 at 3:02 a.m. PST, becoming the first American spacecraft to autonomously dock with the orbiting laboratory.</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="gallery-background gallery-full resize_gallery" data-preload data-desktop="{{assets('theme/static/images/updates/demo-2_article.jpg')}}" data-mobile="{{assets('theme/static/images/updates/demo-2_article.jpg')}}"></div>
						<div class="section-inner resize">
							<div class="inner-left-middle">
								<h3 class="animate">MAY 2020</h3>
								<h2 class="animate">enetki RETURNS HUMAN SPACEFLIGHT TO THE UNITED STATES</h2>
								<p class="animate">Launched atop Falcon 9 on May 30, 2020, Dragon's second demonstration mission to and from the International Space Station, with NASA astronauts onboard the spacecraft, restored human spaceflight to the United States. Later that year, NASA certified enetki’s Falcon 9 and Crew Dragon human spaceflight system for crew missions to and from the space station – becoming the first commercial system in history to achieve such designation.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-timeline"></div>
				<div id="gallery-prev-s" class="gallery-prev swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
					  <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(6) rotate(90)" fill="#fff"/>
					</svg>
				</div>
				<div id="gallery-next-s" class="gallery-next swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewbox="0 0 6 10">
					  <path d="M5,6a.908.908,0,0,1-.7-.3l-4-4A.967.967,0,0,1,.3.3.967.967,0,0,1,1.7.3L5,3.6,8.3.3A.967.967,0,0,1,9.7.3a.967.967,0,0,1,0,1.4l-4,4A.908.908,0,0,1,5,6Z" transform="translate(0 10) rotate(-90)" fill="#fff"/>
					</svg>
				</div>
			</div>
			<script>
				const years = ['', '2008', '2012', '2015', '2016', '2017', '2018', '2019', '2020']
				const history = new Swiper ('#slider', {
					direction: 'horizontal',
					loop: true,
					simulateTouch: false,
					longSwipesRatio: 0.1,
    				shortSwipes: !deviceSettings.isDesktop,
					autoHeight: deviceSettings.isMobile,
					speed: 1500,
					navigation: {
					  nextEl: '#gallery-next-s',
					  prevEl: '#gallery-prev-s',
					},
					pagination: {
		        el: '.swiper-timeline',
		        clickable: false,
		        renderBullet: (index, className) => {
		        	let html = ''
		        	if (index !== 0) {
		        		html = `<div class="swiper-timeline-item" data-index="${index}">
									<span>${years[index]}</span>
								</div>`
		        	}
		          return html
		        }
		      }
				}).on('slideChange', () => {
			    const items = Array.from(document.querySelectorAll('.swiper-timeline-item'))
			    const item = items[history.realIndex - 1]
			    items.forEach((item) => item.classList.remove('is-current'))
			    item && item.classList.add('is-current')
			  })

			  Array.from(document.querySelectorAll('.swiper-timeline-item')).forEach(item => {
			  	item.addEventListener('click', e => {
			  		const index = parseInt(e.currentTarget.getAttribute('data-index')) + 1
			  		history.slideTo(index)
			  	})
			  })
			</script>

			<div class="section">
				<div class="content-columns">
					<div class="collapse left">
						<div class="column-image animate">
							<picture class="responsive-image">
							  <source srcset="{{assets('theme/static/images/content/mission_reusability.jpg')}}" type="image/webp">
							  <source srcset="{{assets('theme/static/images/content/mission_reusability.jpg')}}" type="image/jpeg">
							  <img src="{{assets('theme/static/images/content/mission_reusability.jpg')}}" alt="">
							</picture>
							<video autoplay loop muted class="responsive-video" width="100%" height="100%" preload="auto" autobuffer="true">
								<source type="video/mp4" src="{{assets('theme/static/media/mission_reusability.mp4')}}">
								<source type="video/webm" src="{{assets('theme/static/media/mission_reusability.webm')}}">
							</video>
						</div>
						<div class="inner-right-middle">
							<h2 class="animate">Reusability</h2>
							<p class="animate">enetki believes a fully and rapidly reusable rocket is the pivotal breakthrough needed to substantially reduce the cost of space access. The majority of the launch cost comes from building the rocket, which historically has flown only once.</p>
							<p class="amimate">Compare that to a commercial airliner – each new plane costs about the same as Falcon 9 but can fly multiple times per day and conduct tens of thousands of flights over its lifetime. Following the commercial model, a rapidly reusable space launch vehicle could reduce the cost of traveling to space by a hundredfold.</p>
							<p class="amimate">While most rockets are designed to burn up on reentry, enetki rockets can not only withstand reentry but can also successfully land back on Earth and refly again.</p>
						</div>
					</div>
				</div>
			</div>
			<div id="landing" class="section section--auto">
				<div class="content-center">
					<h2 class="animate">Landing</h2>
					<p class="animate">enetki’s family of Falcon launch vehicles are the first and only orbital class rockets capable of reflight. Depending on the performance required for the mission, Falcon lands on one of our autonomous spaceport droneships out on the ocean or one of our landing zones near our launch pads.</p>
					<div class="content-tabs animate" role="tablist">
						<button role="tab" aria-selected="true" aria-controls="drone-ship-tab-panel" id="drone-ship-tab">Droneship</button>|
						<button role="tab" aria-selected="false" aria-controls="landing-pad-tab-panel" id="landing-pad-tab" tabindex="-1">Landing Zone</button>
					</div>
					<div class="content-tab-data animate" tabindex="0" role="tabpanel" id="drone-ship-tab-panel" aria-labelledby="drone-ship-tab">
						<div class="section-infographics">
							<img style="width: 100%; max-width: 100%; height: auto; margin: auto;" class="infographic-img-desktop" src="{{assets('theme/static/images/infographics/F9_AUTONOMOUS_DRONESHIP_DESKTOP.jpg')}}" alt="">
						</div>
						<div class="section-infographics-mobile">
							<img style="width: 100%; max-width: 100%; height: auto; margin: auto;" class="infographic-img-mobile" src="{{assets('theme/static/images/infographics/F9_AUTONOMOUS_DRONESHIP_MOBILE.jpg')}}" alt="">
						</div>
					</div>
					<div class="content-tab-data animate" tabindex="0" role="tabpanel" id="landing-pad-tab-panel" aria-labelledby="landing-pad-tab" hidden>
						<div class="section-infographics">
							<img style="width: 100%; max-width: 100%; height: auto; margin: auto;" class="infographic-img-desktop" src="{{assets('theme/static/images/infographics/F9_LANDING_ZONE_DESKTOP.jpg')}}" alt="">
						</div>
						<div class="section-infographics-mobile">
							<img style="width: 100%; max-width: 100%; height: auto; margin: auto;" class="infographic-img-mobile" src="{{assets('theme/static/images/infographics/F9_LANDING_ZONE_MOBILE.jpg')}}" alt="">
						</div>
					</div>
				</div>
			</div>

			@include('frontend.includes.footer')
@endsection

