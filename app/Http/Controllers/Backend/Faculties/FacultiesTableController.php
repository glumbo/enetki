<?php

namespace App\Http\Controllers\Backend\Faculties;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Faculties\FacultyRepository;
use App\Http\Requests\Backend\Faculties\ManageFacultyRequest;

/**
 * Class FacultiesTableController.
 */
class FacultiesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var FacultyRepository
     */
    protected $faculty;

    /**
     * contructor to initialize repository object
     * @param FacultyRepository $faculty;
     */
    public function __construct(FacultyRepository $faculty)
    {
        $this->faculty = $faculty;
    }

    /**
     * This method return the data of the model
     * @param ManageFacultyRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageFacultyRequest $request)
    {
        return Datatables::of($this->faculty->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('university', function ($faculty) {
                return $faculty->university?$faculty->university->name:'';
            })
            ->addColumn('created_at', function ($faculty) {
                return Carbon::parse($faculty->created_at)->toDateString();
            })
            ->addColumn('actions', function ($faculty) {
                return $faculty->action_buttons;
            })
            ->make(true);
    }
}
