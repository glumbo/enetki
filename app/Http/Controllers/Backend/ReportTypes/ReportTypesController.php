<?php

namespace App\Http\Controllers\Backend\ReportTypes;

use App\Models\ReportTypes\ReportType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\ReportTypes\CreateResponse;
use App\Http\Responses\Backend\ReportTypes\EditResponse;
use App\Repositories\Backend\ReportTypes\ReportTypeRepository;
use App\Http\Requests\Backend\ReportTypes\ManageReportTypeRequest;
use App\Http\Requests\Backend\ReportTypes\CreateReportTypeRequest;
use App\Http\Requests\Backend\ReportTypes\StoreReportTypeRequest;
use App\Http\Requests\Backend\ReportTypes\EditReportTypeRequest;
use App\Http\Requests\Backend\ReportTypes\UpdateReportTypeRequest;
use App\Http\Requests\Backend\ReportTypes\DeleteReportTypeRequest;

/**
 * ReportTypesController
 */
class ReportTypesController extends Controller
{
    /**
     * variable to store the repository object
     * @var ReportTypeRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param ReportTypeRepository $repository;
     */
    public function __construct(ReportTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\ReportTypes\ManageReportTypeRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageReportTypeRequest $request)
    {
        return new ViewResponse('backend.reporttypes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateReportTypeRequestNamespace  $request
     * @return \App\Http\Responses\Backend\ReportTypes\CreateResponse
     */
    public function create(CreateReportTypeRequest $request)
    {
        return new CreateResponse('backend.reporttypes.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreReportTypeRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreReportTypeRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.reporttypes.index'), ['flash_success' => trans('alerts.backend.reporttypes.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\ReportTypes\ReportType  $reporttype
     * @param  EditReportTypeRequestNamespace  $request
     * @return \App\Http\Responses\Backend\ReportTypes\EditResponse
     */
    public function edit(ReportType $reporttype, EditReportTypeRequest $request)
    {
        return new EditResponse($reporttype);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateReportTypeRequestNamespace  $request
     * @param  App\Models\ReportTypes\ReportType  $reporttype
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateReportTypeRequest $request, ReportType $reporttype)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $reporttype, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.reporttypes.index'), ['flash_success' => trans('alerts.backend.reporttypes.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteReportTypeRequestNamespace  $request
     * @param  App\Models\ReportTypes\ReportType  $reporttype
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(ReportType $reporttype, DeleteReportTypeRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($reporttype);
        //returning with successfull message
        return new RedirectResponse(route('admin.reporttypes.index'), ['flash_success' => trans('alerts.backend.reporttypes.deleted')]);
    }
    
}
