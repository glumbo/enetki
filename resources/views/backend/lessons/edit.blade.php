@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.lessons.management') . ' | ' . _tr('labels.backend.lessons.edit'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.lessons.management') }}
        <small>{{ _tr('labels.backend.lessons.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($lessons, ['route' => ['admin.lessons.update', $lessons], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-lesson','files'=>true]) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.lessons.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.lessons.partials.lessons-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.lessons.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.lessons.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
