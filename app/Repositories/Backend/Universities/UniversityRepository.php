<?php

namespace App\Repositories\Backend\Universities;

use DB;
use Carbon\Carbon;
use App\Models\Universities\University;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UniversityRepository.
 */
class UniversityRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = University::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.universities.table').'.id',
                config('module.universities.table').'.city_id',
                config('module.universities.table').'.name',
                config('module.universities.table').'.short_name',
                config('module.universities.table').'.type',
                config('module.universities.table').'.created_at',
                config('module.universities.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (University::create($input)) {
            return true;
        }
        throw new GeneralException(_tr('exceptions.backend.universities.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param University $university
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(University $university, array $input)
    {
    	if ($university->update($input))
            return true;

        throw new GeneralException(_tr('exceptions.backend.universities.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param University $university
     * @throws GeneralException
     * @return bool
     */
    public function delete(University $university)
    {
        if ($university->delete()) {
            return true;
        }

        throw new GeneralException(_tr('exceptions.backend.universities.delete_error'));
    }
}
