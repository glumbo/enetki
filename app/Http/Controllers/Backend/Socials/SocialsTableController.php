<?php

namespace App\Http\Controllers\Backend\Socials;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Socials\SocialRepository;
use App\Http\Requests\Backend\Socials\ManageSocialRequest;

/**
 * Class SocialsTableController.
 */
class SocialsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var SocialRepository
     */
    protected $social;

    /**
     * contructor to initialize repository object
     * @param SocialRepository $social;
     */
    public function __construct(SocialRepository $social)
    {
        $this->social = $social;
    }

    /**
     * This method return the data of the model
     * @param ManageSocialRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageSocialRequest $request)
    {
        return Datatables::of($this->social->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('type', function ($social) {
                return _tr('labels.backend.socials.table.'.$social->type);
            })
            ->addColumn('status', function ($social) {
                return $social->status?_tr('labels.general.active'):_tr('labels.general.inactive');
            })
            ->addColumn('created_at', function ($social) {
                return Carbon::parse($social->created_at)->toDateString();
            })
            ->addColumn('actions', function ($social) {
                return $social->action_buttons;
            })
            ->make(true);
    }
}
