<?php

namespace App\Models\Teachers\Traits;

/**
 * Class TeacherAttribute.
 */
trait TeacherAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return "<div class=\"btn-group action-btn\"> {$this->getEditButtonAttribute("edit-teacher", "admin.teachers.edit")}
                {$this->getDeleteButtonAttribute("delete-teacher", "admin.teachers.destroy")}
                </div>";
    }
}
