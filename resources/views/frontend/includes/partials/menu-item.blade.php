@if($menuItemType == 'footer')
        <a class="social {{ active_class(isActiveMenuItem($item)) }}" href="{{ getRouteUrl($item->url, $item->url_type) }}" @if(!empty($item->open_in_new_tab) && ($item->open_in_new_tab == 1)) {{ 'target="_blank"' }} @endif>{{ $item->name }}</a>
@elseif($menuItemType == 'navbar-nav')
<li class="nav-item {{ active_class(isActiveMenuItem($item)) }}"><a href="{{ getRouteUrl($item->url, $item->url_type) }}" @if(!empty($item->open_in_new_tab) &&
        ($item->open_in_new_tab == 1)) {{ 'target="_blank"' }} @endif>{{ $item->name }}</a>
        
        @if(!empty($item->children))
                <ul class="dropdown">  
                        {{ renderMenuItems($item->children , 'frontend.includes.partials.menu-item', 'navbar-nav') }}
                </ul>
        @endif
</li>
@else
<li class="@if($menuItemType == 'navbar') nav-item primary @elseif($menuItemType == 'sidebar') nav-item secondary @endif {{ active_class(isActiveMenuItem($item)) }}"><a href="{{ getRouteUrl($item->url, $item->url_type) }}" @if(!empty($item->open_in_new_tab) &&
        ($item->open_in_new_tab == 1)) {{ 'target="_blank"' }} @endif>{{ $item->name }}</a></li>
@endif