<div class="box-body">
    <div class="form-group">
       {{ Form::label('code', _tr('labels.backend.countries.table.code'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::text('code', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.countries.table.code'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
       {{ Form::label('phone_code', _tr('labels.backend.countries.table.phone_code'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::text('phone_code', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.countries.table.phone_code'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
       {{ Form::label('name', _tr('labels.backend.countries.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.countries.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
