<div class="box-body">
    <div class="form-group">
        {{ Form::label('first_name', trans('labels.backend.contacts.table.first_name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('first_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.contacts.table.first_name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('last_name', trans('labels.backend.contacts.table.last_name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('last_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.contacts.table.last_name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('email', trans('labels.backend.contacts.table.email'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::email('email', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.contacts.table.email'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('subject', trans('labels.backend.contacts.table.subject'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('subject', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.contacts.table.subject'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('message', trans('labels.backend.contacts.table.message'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => trans('labels.backend.contacts.table.message')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        $( document ).ready( function() {
            Backend.Contacts.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
        });
    </script>
@endsection
