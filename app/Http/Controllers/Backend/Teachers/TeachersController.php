<?php

namespace App\Http\Controllers\Backend\Teachers;

use App\Models\Teachers\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Teachers\CreateResponse;
use App\Http\Responses\Backend\Teachers\EditResponse;
use App\Repositories\Backend\Teachers\TeacherRepository;
use App\Http\Requests\Backend\Teachers\ManageTeacherRequest;
use App\Http\Requests\Backend\Teachers\CreateTeacherRequest;
use App\Http\Requests\Backend\Teachers\StoreTeacherRequest;
use App\Http\Requests\Backend\Teachers\EditTeacherRequest;
use App\Http\Requests\Backend\Teachers\UpdateTeacherRequest;
use App\Http\Requests\Backend\Teachers\DeleteTeacherRequest;

/**
 * TeachersController
 */
class TeachersController extends Controller
{
    /**
     * variable to store the repository object
     * @var TeacherRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param TeacherRepository $repository;
     */
    public function __construct(TeacherRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Teachers\ManageTeacherRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTeacherRequest $request)
    {
        return new ViewResponse('backend.teachers.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateTeacherRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Teachers\CreateResponse
     */
    public function create(CreateTeacherRequest $request)
    {
        return new CreateResponse('backend.teachers.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTeacherRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTeacherRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token','remove_file']);
        if($request->hasFile('cover')){
            $input['cover']=$request->cover->store('img/teachers','public');
        }
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.teachers.index'), ['flash_success' => _tr('alerts.backend.teachers.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Teachers\Teacher  $teacher
     * @param  EditTeacherRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Teachers\EditResponse
     */
    public function edit(Teacher $teacher, EditTeacherRequest $request)
    {
        return new EditResponse($teacher);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTeacherRequestNamespace  $request
     * @param  App\Models\Teachers\Teacher  $teacher
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateTeacherRequest $request, Teacher $teacher)
    {
        //Input received from the request
        $input = $request->except(['_token','remove_file']);
        if($request->hasFile('cover')){
            $input['cover']=$request->cover->store('img/teachers','public');
        }
        if($request->remove_file){
            $input['cover'] = NULL;
        }
        //Update the model using repository update method
        $this->repository->update( $teacher, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.teachers.index'), ['flash_success' => _tr('alerts.backend.teachers.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteTeacherRequestNamespace  $request
     * @param  App\Models\Teachers\Teacher  $teacher
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Teacher $teacher, DeleteTeacherRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($teacher);
        //returning with successfull message
        return new RedirectResponse(route('admin.teachers.index'), ['flash_success' => _tr('alerts.backend.teachers.deleted')]);
    }
    
}
