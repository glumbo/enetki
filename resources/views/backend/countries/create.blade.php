@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.countries.management') . ' | ' . _tr('labels.backend.countries.create'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.countries.management') }}
        <small>{{ _tr('labels.backend.countries.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.countries.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-country']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.countries.create') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.countries.partials.countries-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.countries.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.countries.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!-- form-group -->
            </div><!--box-body-->
        </div><!--box box-success-->
    {{ Form::close() }}
@endsection
