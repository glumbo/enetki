@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.sliders.management') . ' | ' . _tr('labels.backend.sliders.create'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.sliders.management') }}
        <small>{{ _tr('labels.backend.sliders.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.sliders.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-slider','files'=>true]) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.sliders.create') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.sliders.partials.sliders-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.sliders.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.sliders.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!-- form-group -->
            </div><!--box-body-->
        </div><!--box box-success-->
    {{ Form::close() }}
@endsection
