<?php

namespace App\Models\ReportTypes\Traits;

/**
 * Class ReportTypeAttribute.
 */
trait ReportTypeAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return "<div class=\"btn-group action-btn\"> {$this->getEditButtonAttribute("edit-reporttype", "admin.reporttypes.edit")}
                {$this->getDeleteButtonAttribute("delete-reporttype", "admin.reporttypes.destroy")}
                </div>";
    }
}
