<?php

namespace App\Http\Controllers\Backend\Faculties;

use App\Models\Faculties\Faculty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Faculties\CreateResponse;
use App\Http\Responses\Backend\Faculties\EditResponse;
use App\Repositories\Backend\Faculties\FacultyRepository;
use App\Http\Requests\Backend\Faculties\ManageFacultyRequest;
use App\Http\Requests\Backend\Faculties\CreateFacultyRequest;
use App\Http\Requests\Backend\Faculties\StoreFacultyRequest;
use App\Http\Requests\Backend\Faculties\EditFacultyRequest;
use App\Http\Requests\Backend\Faculties\UpdateFacultyRequest;
use App\Http\Requests\Backend\Faculties\DeleteFacultyRequest;

/**
 * FacultiesController
 */
class FacultiesController extends Controller
{
    /**
     * variable to store the repository object
     * @var FacultyRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param FacultyRepository $repository;
     */
    public function __construct(FacultyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Faculties\ManageFacultyRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageFacultyRequest $request)
    {
        return new ViewResponse('backend.faculties.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateFacultyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Faculties\CreateResponse
     */
    public function create(CreateFacultyRequest $request)
    {
        return new CreateResponse('backend.faculties.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreFacultyRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreFacultyRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.faculties.index'), ['flash_success' => _tr('alerts.backend.faculties.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Faculties\Faculty  $faculty
     * @param  EditFacultyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Faculties\EditResponse
     */
    public function edit(Faculty $faculty, EditFacultyRequest $request)
    {
        return new EditResponse($faculty);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateFacultyRequestNamespace  $request
     * @param  App\Models\Faculties\Faculty  $faculty
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateFacultyRequest $request, Faculty $faculty)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $faculty, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.faculties.index'), ['flash_success' => _tr('alerts.backend.faculties.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteFacultyRequestNamespace  $request
     * @param  App\Models\Faculties\Faculty  $faculty
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Faculty $faculty, DeleteFacultyRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($faculty);
        //returning with successfull message
        return new RedirectResponse(route('admin.faculties.index'), ['flash_success' => _tr('alerts.backend.faculties.deleted')]);
    }
    
}
