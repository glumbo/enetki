<?php

namespace App\Http\Responses\Backend\Socials;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Socials\Social
     */
    protected $socials;

    /**
     * @param App\Models\Socials\Social $socials
     */
    public function __construct($socials)
    {
        $this->socials = $socials;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $types = [
            'facebook' => _tr('labels.backend.socials.table.facebook'),
            'twitter' => _tr('labels.backend.socials.table.twitter'),
            'linkedin' => _tr('labels.backend.socials.table.linkedin'),
            'youtube' => _tr('labels.backend.socials.table.youtube'),
            'pinterest' => _tr('labels.backend.socials.table.pinterest'),
        ];
        $status = [
            0 => _tr('labels.general.inactive'),
            1 => _tr('labels.general.active'),
        ];
        return view('backend.socials.edit')->with([
            'types' => $types,
            'status' => $status,
            'socials' => $this->socials
        ]);
    }
}