<div class="box-body">
    <div class="form-group">
        {{ Form::label('cover', _tr('labels.backend.comments.table.cover'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($comments->cover))
            <div class="col-lg-1 file_show">
                <img src="{{ Storage::disk('public')->url($comments->cover) }}" height="80" width="80">
                <i class="fal fa-times remove_file"></i>
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                    <input type="hidden" name="remove_file" id="remove_file" value="0" />
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('name', trans('labels.backend.testimonials.table.name'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.testimonials.table.name')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('title', trans('labels.backend.testimonials.table.title'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.testimonials.table.title')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('comment', trans('labels.backend.testimonials.table.comment'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => trans('labels.backend.testimonials.table.comment')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            Backend.Testimonials.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
        });
    </script>
@endsection
