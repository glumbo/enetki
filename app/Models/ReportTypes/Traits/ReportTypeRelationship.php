<?php

namespace App\Models\ReportTypes\Traits;
use App\Models\ReportTypes\ReportType;

/**
 * Class ReportTypeRelationship
 */
trait ReportTypeRelationship
{

    public function parent()
    {
        return $this->belongsTo(ReportType::class,'parent_id');
    }
    public function children()
    {
        return $this->hasMany(ReportType::class,'parent_id','id');
    }
}
