<div class="box-body">
    <div class="form-group {{ $errors->has('country_id') ? ' has-error' : '' }}">
        {{ Form::label('country_id', _tr('labels.backend.cities.table.country'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('country_id', $countries, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.cities.table.country'), 'required' => 'required']) }}
           @if ($errors->has('country_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('country_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('plate_no', _tr('labels.backend.cities.table.plate_no'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::number('plate_no', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.cities.table.plate_no'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('phone_code', _tr('labels.backend.cities.table.phone_code'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('phone_code', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.cities.table.phone_code'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('name', _tr('labels.backend.cities.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.cities.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
