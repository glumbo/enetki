<?php

namespace App\Http\Responses\Backend\Lessons;

use Illuminate\Contracts\Support\Responsable;
use App\Models\Lessons\Lesson;
use App\Models\Departments\Department;
use App\Models\Videos\Video;
use App\Models\Teachers\Teacher;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Lessons\Lesson
     */
    protected $lessons;

    /**
     * @param App\Models\Lessons\Lesson $lessons
     */
    public function __construct($lessons)
    {
        $this->lessons = $lessons;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $parents=collect(Lesson::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $departments=collect(Department::where('status',1)->get())->mapWithKeys(function ($item) {
            return [$item['id'] => $item->faculty->university->short_name.' - '.$item['name']];
        });
        $videos=collect(Video::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $teachers=collect(Teacher::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title'].' '.$item['first_name'].' '.$item['last_name']];
        });
        return view('backend.lessons.edit')->with([
            'lessons' => $this->lessons,
            'parents' => $parents,
            'departments' => $departments,
            'videos' => $videos,
            'teachers' => $teachers
        ]);
    }
}