<?php

namespace App\Models\Lessons\Traits;
use App\Models\Lessons\Lesson;
use App\Models\Departments\Department;
use App\Models\Videos\Video;
use App\Models\Teachers\Teacher;

/**
 * Class LessonRelationship
 */
trait LessonRelationship
{
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
    public function parent()
    {
        return $this->belongsTo(Lesson::class,'parent_id');
    }
    public function children()
    {
        return $this->hasMany(Lesson::class,'parent_id','id');
    }
    public function video()
    {
        return $this->belongsTo(Video::class,'video_id');
    }
    public function teacher()
    {
        return $this->belongsTo(Teacher::class,'teacher_id');
    }
}
