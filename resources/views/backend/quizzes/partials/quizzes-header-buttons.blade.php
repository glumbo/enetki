<!--Action Button-->
@if( Active::checkUriPattern( 'admin/quizzes' ) )
    <div class="btn-group">
        <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">Export
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li id="copyButton"><a href="#"><i class="fal fa-clone"></i> Copy</a></li>
            <li id="csvButton"><a href="#"><i class="fal fa-file-text"></i> CSV</a></li>
            <li id="excelButton"><a href="#"><i class="fal fa-file-excel"></i> Excel</a></li>
            <li id="pdfButton"><a href="#"><i class="fal fa-file-pdf"></i> PDF</a></li>
            <li id="printButton"><a href="#"><i class="fal fa-print"></i> Print</a></li>
        </ul>
    </div>
@endif
<!--Action Button-->
<div class="btn-group">
    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Action
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{{ route( 'admin.quizzes.index' ) }}">
                <i class="fal fa-list-ul"></i> {{ _tr('menus.backend.quizzes.all' ) }}
            </a>
        </li>
        @permission( 'create-quiz' )
            <li>
                <a href="{{ route( 'admin.quizzes.create' ) }}">
                    <i class="far fa-plus"></i> {{ _tr('menus.backend.quizzes.create' ) }}
                </a>
            </li>
        @endauth
    </ul>
</div>
<div class="clearfix"></div>
