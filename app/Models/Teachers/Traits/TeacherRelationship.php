<?php

namespace App\Models\Teachers\Traits;
use App\Models\Departments\Department;
use App\Models\Lessons\Lesson;

/**
 * Class TeacherRelationship
 */
trait TeacherRelationship
{
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
    public function lessons()
    {
        return $this->hasMany(Lesson::class,'teacher_id');
    }
}
