<?php

namespace App\Http\Responses\Backend\Questions;

use Illuminate\Contracts\Support\Responsable;
use App\Models\Lessons\Lesson;
use App\Models\Videos\Video;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Questions\Question
     */
    protected $questions;

    /**
     * @param App\Models\Questions\Question $questions
     */
    public function __construct($questions)
    {
        $this->questions = $questions;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $videos=collect(Video::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $lessons=collect(Lesson::all()->toArray())->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $types = [
            0 => _tr('labels.backend.questions.table.example'), 
            1 => _tr('labels.backend.questions.table.quiz'), 
        ];
        $options = [
            'a' => _tr('labels.backend.questions.table.a'), 
            'b' => _tr('labels.backend.questions.table.b'), 
            'c' => _tr('labels.backend.questions.table.c'), 
            'd' => _tr('labels.backend.questions.table.d'), 
            'e' => _tr('labels.backend.questions.table.e'), 
        ];
        return view('backend.questions.edit')->with([
            'questions' => $this->questions,
            'videos' => $videos,
            'lessons' => $lessons,
            'types' => $types,
            'options' => $options,
        ]);
    }
}