@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.teachers.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.teachers.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.teachers.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.teachers.partials.teachers-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="teachers-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.teachers.table.id') }}</th>
                            <th>{{ _tr('labels.backend.teachers.table.department') }}</th>
                            <th>{{ _tr('labels.backend.teachers.table.cover') }}</th>
                            <th>{{ _tr('labels.backend.teachers.table.name') }}</th>
                            <th>{{ _tr('labels.backend.teachers.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#teachers-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.teachers.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.teachers.table')}}.id'},
                    {data: 'department', name: '{{config('module.teachers.table')}}.department_id'},
                    {data: 'cover', name: '{{config('module.teachers.table')}}.cover'},
                    {data: 'name', name: '{{config('module.teachers.table')}}.name'},
                    {data: 'created_at', name: '{{config('module.teachers.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
