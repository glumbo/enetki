<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $table = 'core_platforms';
}
