@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.words.management') . ' | ' . _tr('labels.backend.words.create'))

@section('page-header')
    <h1>
        {{ _tr('labels.backend.words.management') }}
        <small>{{ _tr('labels.backend.words.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.words.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-word']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ _tr('labels.backend.words.create') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.words.partials.words-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.words.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.words.index', _tr('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(_tr('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!-- form-group -->
            </div><!--box-body-->
        </div><!--box box-success-->
    {{ Form::close() }}
@endsection
