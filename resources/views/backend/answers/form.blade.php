<div class="box-body">
    <div class="form-group {{ $errors->has('quiz_id') ? ' has-error' : '' }}">
        {{ Form::label('quiz_id', _tr('labels.backend.answers.table.quiz'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('quiz_id', $quizzes, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.answers.table.quiz'), 'required' => 'required']) }}
           @if ($errors->has('quiz_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('quiz_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
        {{ Form::label('user_id', _tr('labels.backend.answers.table.user'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('user_id', $users, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.answers.table.user'), 'required' => 'required']) }}
           @if ($errors->has('user_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('question_id') ? ' has-error' : '' }}">
        {{ Form::label('question_id', _tr('labels.backend.answers.table.question'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('question_id', $questions, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.answers.table.question'), 'required' => 'required']) }}
           @if ($errors->has('question_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('question_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('answer') ? ' has-error' : '' }}">
        {{ Form::label('answer', _tr('labels.backend.answers.table.answer'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('answer', $questions, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.answers.table.answer'), 'required' => 'required']) }}
           @if ($errors->has('answer'))
                <span class="help-block">
                    <strong>{{ $errors->first('answer') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
