<?php

namespace App\Http\Controllers\Backend\Questions;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Questions\QuestionRepository;
use App\Http\Requests\Backend\Questions\ManageQuestionRequest;

/**
 * Class QuestionsTableController.
 */
class QuestionsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var QuestionRepository
     */
    protected $question;
    protected $types;

    /**
     * contructor to initialize repository object
     * @param QuestionRepository $question;
     */
    public function __construct(QuestionRepository $question)
    {
        $this->question = $question;
        $this->types = [
            0 => _tr('labels.backend.questions.table.example'), 
            1 => _tr('labels.backend.questions.table.quiz'), 
        ];
    }

    /**
     * This method return the data of the model
     * @param ManageQuestionRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageQuestionRequest $request)
    {
        return Datatables::of($this->question->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('lesson', function ($question) {
                return $question->lesson?$question->lesson->name:'';
            })
            ->addColumn('video', function ($question) {
                return $question->video?$question->video->name:'';
            })
            ->addColumn('type', function ($question) {
                return $this->types[$question->type]?$this->types[$question->type]:'';
            })
            ->addColumn('created_at', function ($question) {
                return Carbon::parse($question->created_at)->toDateString();
            })
            ->addColumn('actions', function ($question) {
                return $question->action_buttons;
            })
            ->make(true);
    }
}
