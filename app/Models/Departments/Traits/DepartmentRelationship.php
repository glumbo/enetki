<?php

namespace App\Models\Departments\Traits;
use App\Models\Faculties\Faculty;
/**
 * Class DepartmentRelationship
 */
trait DepartmentRelationship
{
    public function faculty()
    {
        return $this->belongsTo(Faculty::class,'faculty_id');
    }
}
