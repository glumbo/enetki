<div class="box-body">
    @if($parents->count()>0)
    <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
        {{ Form::label('parent_id', _tr('labels.backend.lessons.table.parent'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('parent_id', $parents, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.lessons.table.parent')]) }}
           @if ($errors->has('parent_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    @endif
    <div class="form-group {{ $errors->has('department_id') ? ' has-error' : '' }}">
        {{ Form::label('department_id', _tr('labels.backend.lessons.table.department'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('department_id', $departments, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.lessons.table.department'), 'required' => 'required']) }}
           @if ($errors->has('department_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('department_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('teacher_id') ? ' has-error' : '' }}">
        {{ Form::label('teacher_id', _tr('labels.backend.lessons.table.teacher'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('teacher_id', $teachers, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.lessons.table.teacher'), 'required' => 'required']) }}
           @if ($errors->has('teacher_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('teacher_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group {{ $errors->has('video_id') ? ' has-error' : '' }}">
        {{ Form::label('video_id', _tr('labels.backend.lessons.table.video'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
           {{ Form::select('video_id', $videos, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.lessons.table.video')]) }}
           @if ($errors->has('video_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('video_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    
    <div class="form-group">
        {{ Form::label('name', _tr('labels.backend.lessons.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.lessons.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('slug', _tr('labels.backend.lessons.table.slug'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('slug', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.lessons.table.slug'), 'disabled' => 'disabled']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('cover', _tr('labels.backend.lessons.table.cover'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($lessons->cover))
            <div class="col-lg-1 file_show">
                <img src="{{ Storage::disk('public')->url($lessons->cover) }}" height="80" width="80">
                <i class="fal fa-times remove_file"></i>
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                    <input type="hidden" name="remove_file" id="remove_file" value="0" />
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('description', _tr('labels.backend.lessons.table.description'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10 mce-box">
            {{ Form::textarea('description', null,['class' => 'form-control', 'placeholder' => _tr('labels.backend.lessons.table.description')]) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('price', _tr('labels.backend.lessons.table.price'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('price', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.lessons.table.price'), 'step'=>'0.01']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">

        Backend.Lessons.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
        Backend.Lessons.selectors.SlugUrl = "{{url('/')}}";
        Backend.Lessons.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');

    </script
@endsection
