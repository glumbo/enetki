@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.universities.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.universities.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.universities.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.universities.partials.universities-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="universities-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.universities.table.id') }}</th>
                            <th>{{ _tr('labels.backend.universities.table.city') }}</th>
                            <th>{{ _tr('labels.backend.universities.table.short_name') }}</th>
                            <th>{{ _tr('labels.backend.universities.table.name') }}</th>
                            <th>{{ _tr('labels.backend.universities.table.type') }}</th>
                            <th>{{ _tr('labels.backend.universities.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#universities-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.universities.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.universities.table')}}.id'},
                    {data: 'city', name: '{{config('module.universities.table')}}.city_id'},
                    {data: 'short_name', name: '{{config('module.universities.table')}}.short_name'},
                    {data: 'name', name: '{{config('module.universities.table')}}.name'},
                    {data: 'type', name: '{{config('module.universities.table')}}.type'},
                    {data: 'created_at', name: '{{config('module.universities.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
