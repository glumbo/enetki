<div class="box-body">
    <div class="form-group">
        {{ Form::label('cover', _tr('labels.backend.references.table.cover'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($references->cover))
            <div class="col-lg-1 file_show">
                <img src="{{ Storage::disk('public')->url($references->cover) }}" height="80" width="80">
                <i class="fal fa-times remove_file"></i>
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                    <input type="hidden" name="remove_file" id="remove_file" value="0" />
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="cover" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fal fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('name', trans('labels.backend.references.table.name'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.references.table.name')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
    <div class="form-group">
        {{ Form::label('link', trans('labels.backend.references.table.link'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('link', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.references.table.link')]) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
