<div class="box-body">
    <div class="form-group {{ $errors->has('university_id') ? ' has-error' : '' }}">
        {{ Form::label('university_id', _tr('labels.backend.faculties.table.university'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('university_id', $universities, null, ['class' => 'form-control select2 status box-size', 'placeholder' => _tr('labels.backend.faculties.table.university'), 'required' => 'required']) }}
           @if ($errors->has('university_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('university_id') }}</strong>
                </span>
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('name', _tr('labels.backend.faculties.table.name'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => _tr('labels.backend.faculties.table.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
