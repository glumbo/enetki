<?php

namespace App\Models\Quizzes\Traits;
use App\Models\Questions\Question;

/**
 * Class QuizRelationship
 */
trait QuizRelationship
{
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }
}
