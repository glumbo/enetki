@extends ('backend.layouts.app')

@section ('title', _tr('labels.backend.answers.management'))

@section('page-header')
    <h1>{{ _tr('labels.backend.answers.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ _tr('labels.backend.answers.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.answers.partials.answers-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="answers-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ _tr('labels.backend.answers.table.id') }}</th>
                            <th>{{ _tr('labels.backend.answers.table.quiz') }}</th>
                            <th>{{ _tr('labels.backend.answers.table.question') }}</th>
                            <th>{{ _tr('labels.backend.answers.table.user') }}</th>
                            <th>{{ _tr('labels.backend.answers.table.answer') }}</th>
                            <th>{{ _tr('labels.backend.answers.table.createdat') }}</th>
                            <th>{{ _tr('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#answers-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.answers.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.answers.table')}}.id'},
                    {data: 'quiz', name: '{{config('module.answers.table')}}.quiz_id'},
                    {data: 'question', name: '{{config('module.answers.table')}}.question_id'},
                    {data: 'user', name: '{{config('module.answers.table')}}.user_id'},
                    {data: 'answer', name: '{{config('module.answers.table')}}.answer'},
                    {data: 'created_at', name: '{{config('module.answers.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns:'th:not(:last-child)'  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns:'th:not(:last-child)'  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
