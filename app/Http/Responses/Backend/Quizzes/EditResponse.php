<?php

namespace App\Http\Responses\Backend\Quizzes;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Quizzes\Quiz
     */
    protected $quizzes;

    /**
     * @param App\Models\Quizzes\Quiz $quizzes
     */
    public function __construct($quizzes)
    {
        $this->quizzes = $quizzes;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.quizzes.edit')->with([
            'quizzes' => $this->quizzes
        ]);
    }
}